
#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/select.h> 

#define MSG 1024



struct ip_mreq {
  struct in_addr imr_multiaddr; /* multicast group to join */
  struct in_addr imr_interface; /* interface to join one */
};

void ecoute(int sock){
  /* va afficher le message recu */
  char message[MSG];
  do{

    if(recvfrom(sock,message,MSG,0,NULL,NULL)==-1){
      perror("revfrom");
      exit(1);
    }
    printf("%s\n",message);

  }while(1);
}

void writer(int sock, char *addr, char* port, char *nom){
  char message[MSG],chat[MSG];
  struct sockaddr_in dest;

  memset((void*)&dest,0,sizeof(dest));
  dest.sin_family=AF_INET;
  dest.sin_addr.s_addr=inet_addr(addr); /* sa ne marchait pas à cause de ça cétait htonl(atoi(addr)) ??? c quoi la diff */ 
  dest.sin_port=htons(atoi(port));

  do{

    memset(message,'\0',sizeof(message));

    if(read(STDIN_FILENO, message,sizeof(message))==-1){
      perror("read");
      exit(1);
    }

    sprintf(chat,"%s: %s\n", nom, message);
   
    if(sendto(sock,chat,strlen(chat)+1,0,(struct sockaddr *)&dest,sizeof(dest))==-1){
      perror("sendto");
      exit(1);
    }


  }while(1);

}



int main(int argc, char **argv){

  int sock,yes=1,pid;
  struct ip_mreq imr;
  struct sockaddr_in addrServ;

  if(argc!=4){
    printf("usage: %s <@IP multicast> <port> <pseudo>\n",argv[0]);
    exit(1);
  }

  imr.imr_multiaddr.s_addr=inet_addr(argv[1]);
  imr.imr_interface.s_addr=INADDR_ANY;

  if((sock=socket(AF_INET,SOCK_DGRAM,0))==-1){
    perror("socket");
    exit(1);
  }

  if(setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes))==-1){
    perror("SO_REUSEADDR");
    exit(1);
  }

  if(setsockopt(sock,IPPROTO_IP,IP_ADD_MEMBERSHIP,(char*)&imr,sizeof(imr))==-1){
    perror("SO_MEMBERSHIP");
    exit(1);
  }

  memset((void*)&addrServ,0,sizeof(addrServ));
  addrServ.sin_family=AF_INET;
  addrServ.sin_addr.s_addr=htonl(INADDR_ANY);
  addrServ.sin_port=htons(atoi(argv[2]));

  if(bind(sock,(struct sockaddr *)&addrServ,sizeof(addrServ))==-1){
    perror("bind");
    exit(1);
  }

  if((pid=fork()==0))ecoute(sock);
  else if(pid==-1){
    perror("fork 1");
    exit(1);
  }

  pid=0;
  if((pid=fork()==0))writer(sock,argv[1],argv[2],argv[3]);
  else if(pid==-1){
    perror("fork 1");
    exit(1);
  }

  wait(NULL);
  wait(NULL);

  return EXIT_SUCCESS;
}
