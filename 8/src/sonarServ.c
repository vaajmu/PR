#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/select.h>
#include <pthread.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 9999
#define PING 10


void* waitAnswer(void* sc){

  int sock,fromlen;
  char host[64], pong[PING];
  struct sockaddr_in exp;

  sock=*(int*)sc;

  printf("\t\t\t\t\tthread waiting answer \n");

  fromlen=sizeof(exp);

  do{
    printf("\t\t\t\t\t attente de reception d'un pong\n");
    if(recvfrom(sock, pong, PING+1, 0, (struct sockaddr *)&exp, &fromlen)==-1){
      perror("\t\t\t\t\t waitAnswer, recvfrom");
      exit(1);
    }

    if(strcmp(pong,"pong\0")==0){

      /* get ip and port of exp */
      printf("\t\t\t\t\t Exp : <IP = %s, PORT= %d>\n",inet_ntoa(exp.sin_addr),ntohs(exp.sin_port));
    
      /* get name of exp */
      if(getnameinfo((struct sockaddr*)&exp,sizeof(exp),host,sizeof(host),0,0,0)!=0){
	perror("\t\t\t\t\t getnameinfo");
	exit(3);
      }

      printf("\t\t\t\t\t Machine %s\n",host);
      printf("\t\t\t\t\t Message : %s\n",pong);

    }

  }while(1);
}

void* broadcasting(void* sc){
  int sock;
  char ping[PING]="ping\0";
  fd_set mselect;

  struct sockaddr_in broadcast;

  struct timeval timeout;

  sock=*(int*)sc;
  printf("thread of broadcasting num of socket= %d \n",sock);

  //printf("INFO Client: \n PORT %d\n @IP %s\n", PORT, INADDR_ANY);

  /* remplir la structure broadcast (dest) */
  memset(&broadcast,0,sizeof(broadcast));
  broadcast.sin_addr.s_addr=htonl(INADDR_BROADCAST);
  broadcast.sin_port=htons(PORT);
  broadcast.sin_family=AF_INET;

  //printf("@ broadcast : %s\n", INADDR_BROADCAST);

  do{
    timeout.tv_sec=3;
    timeout.tv_usec=0;

    FD_ZERO(&mselect);
    FD_SET(sock,&mselect);

    printf("attente de la socket pendent %d sec\n",(int)timeout.tv_sec);

    if(select(sock+1, &mselect, NULL,NULL,&timeout)==-1){
      perror("select");
      exit(1);
    }

    //    if(FD_ISSET(sock,&mselect)){ 
    sleep(3);
    if( sendto(sock,ping,strlen(ping)+1,0,(struct sockaddr *)&broadcast,sizeof(broadcast))==-1 ){
      perror("broadcasting, sendto");
      exit(1);
    }
      printf("broadcasting !!\n");
      //}
      
      printf("\t\t\t %s done\n",ping);

  }while(1);
  
  pthread_exit(NULL);
}

int main(int argc, char **argv){

  struct sockaddr_in addrServ;

  pthread_t connexion , diffusion;
  int sockServ,yes=1;

  if(argc!=1){
    printf("%s  ne prends pas d'argument ! \n",argv[0]);
    exit(1);
  }

  if((sockServ=socket(AF_INET,SOCK_DGRAM,0))==-1){
    perror("socket");
    exit(1);
  }


  if(setsockopt(sockServ,SOL_SOCKET, SO_REUSEADDR, &yes,sizeof(yes))==-1){
    perror("setsockopt, IPPROTO");
    exit(1);
  }


  if(setsockopt(sockServ,SOL_SOCKET, SO_BROADCAST, &yes , sizeof(yes))==-1){
    perror("setsockopt IPPROTO_IP");
    exit(1);
  } 

  memset((void*)&addrServ,0,sizeof(struct sockaddr_in));
  addrServ.sin_family=AF_INET;
  addrServ.sin_port=htons(PORT);
  addrServ.sin_addr.s_addr=htonl(INADDR_ANY);

  if(bind(sockServ,(struct sockaddr *)&addrServ,sizeof(addrServ))==-1){
    perror("bind");
    exit(1);
  }

  printf("création de la thread broadcasting\n");
  if(pthread_create(&diffusion,NULL, broadcasting,&sockServ)==-1){
    perror("pthread create");
    exit(1);
  }

  printf("création de la thread wait answer\n");
  if(pthread_create(&connexion,NULL, waitAnswer,&sockServ)==-1){
    perror("pthread create");
    exit(1);
  }

  sleep(60);	  /* temps d'execution des thread en sec */

  printf("annulation des thread\n");
  if(pthread_cancel(diffusion)==-1){
    perror("pthread_cancel");
    exit(1);
  }

  if(pthread_cancel(connexion)==-1){
    perror("pthread_cancel");
    exit(1);
  }

  printf("attente de la terminaison des thread\n");
  if(pthread_join(diffusion,NULL)==-1){
    perror("join diffusion");
    exit(1);
  }

  if(pthread_join(connexion,NULL)==-1){
    perror("join connexion");
    exit(1);
  }

  if(close(sockServ)==-1){
    perror("close sock Serv");
    exit(1);
  }

  return EXIT_SUCCESS;
}
