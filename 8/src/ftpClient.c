#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <dirent.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <arpa/inet.h>

#define TAILLEBUF 1024

/* Si un appel de fonction échoue les fonctions upload, download, list
   et envoiCommandes provoque la terminaison de la fonction et de la
   fonction appelante => Un échec termine le programme */

/* Les fichiers sur le serveur ne peuvent pas avoir d'espaces */

/* Optimisations : */
/*     Certains échecs ne devraient pas terminer le prg. Par exemple
       dans download, si le nouveau fichier ne peut pas être ouvert,
       la fonction peut renvoyer 0 (fin normale) ou -2 (échec sans
       entraîner la terminaison du programme) plutôt que -1 */

int download(int sfd, char *nom) {
  int lenFic, retourRead, ffd, cpt, ok;
  char buf[TAILLEBUF], *cible = NULL;

  printf("\t\t\t\tAppel download\n");

  /* Originalement, l'argument nom est lachaine nomSurServeur cheminChezClient */
  if((cible = strchr(nom, ' ')) == NULL) {
    printf("Erreur commande\n");
    return -1;
  }
  /* Maintenant nom est seulement le nom sur le serveur */
  /* et la cible est le chemin chez le client */
  *cible++ = '\0';
  
  /* Ouverture fichier */
  if((ffd = open(cible, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1) {
    perror("open ");
    return -1;
  }

  /* Envoi de la commande */
  if(sprintf(buf, "DOWNLOAD %s", nom) < 0) {
    perror("sprintf ");
    return -1;
  }
  if(write(sfd, buf, strlen(buf)+1) == -1) {
    perror("write ");
    return -1;
  }
  if(read(sfd, &ok, sizeof(ok)) == -1) {
    perror("read ");
    return -1;
  }

  /* lecture taille fichier */
  if(read(sfd, &lenFic, sizeof(lenFic)) == -1) {
    perror("read ");
    return -1;
  }
  
  cpt = lenFic;
  /* Reception fichier et écriture */
  while(cpt > 0) {
    if((retourRead = read(sfd, buf, TAILLEBUF)) == -1) {
      perror("read ");
      return -1;
    }

    cpt -= retourRead;
    if(write(ffd, buf, retourRead) == -1) {
      perror("write ");
      return -1;
    }
  }

  /* Fermeture fichier */
  if(close(ffd) == -1) {
    perror("close ");
    /* return -1; */
  }

  printf("\t\t\t\tFin download\n");

  return 0;
}

int upload(int sfd, char *nom) {
  int ffd, cpt = 0, nbEnv, ok;
  char *contenu, *cible = NULL, *commande = NULL;
  struct stat stats;
  
  printf("\t\t\t\tAppel upload\n");
  /* Originalement, l'argument nom est la chaine nomSurServeur cheminChezClient */
  if((cible = strchr(nom, ' ')) == NULL) {
    printf("Erreur commande\n");
    return -1;
  }
  /* Maintenant nom est seulement le nom sur le serveur */
  /* et cible est le chemin chez le client */
  *cible++ = '\0';
  

  /* Utiliser fstat après le open ?? */
  if(stat(cible, &stats) == -1) {
    perror("stat ");
    return -1;
  }

  if((ffd = open(cible, O_RDONLY)) == -1) {
    perror("open ");
    return -1;
  }

  if((contenu = mmap(NULL, stats.st_size, PROT_READ, MAP_PRIVATE, ffd, 0)) == MAP_FAILED) {
    perror("mmap ");
    return -1;
  }

  /* Envoi de la commande */
  if((commande = (char*)malloc(sizeof(char)*( strlen(nom)+8 ))) == NULL) {
    perror("malloc ");
    return -1;
  }
  if(sprintf(commande, "UPLOAD %s", nom) < 0) {
    perror("sprintf ");
    return -1;
  }

  if(write(sfd, commande, strlen(commande)+1) == -1) {
    perror("write ");
    return -1;
  }
  if(read(sfd, &ok, sizeof(ok)) == -1) {
    perror("read ");
    return -1;
  }

  free(commande);

  printf("\t\t\t\t\t\ttaille : %ld\n", (long)stats.st_size);
  /* Envoi de la taille du fichier */
  if(write(sfd, &(stats.st_size), sizeof(stats.st_size)) == -1) {
    perror("write ");
    return -1;
  }

  /* Envoi du fichier */
  while(cpt != stats.st_size) {
    if((nbEnv = write(sfd, contenu+cpt, stats.st_size - cpt)) == -1) {
      perror("write");
      return -1;
    }
    cpt += nbEnv;
  }  

  /* fermeture fichier */
  if(munmap(contenu, stats.st_size) == -1) {
    perror("munmap ");
    return -1;
  }
  if(close(ffd) == -1) {
    perror("close ");
    return -1;
  }

  printf("\t\t\t\tFin upload\n");
  return 0;
}

int list(int sfd) {
  char cmd[] = "LIST", buf[TAILLEBUF];
  int ok, len;

  printf("\t\t\t\tAppel list\n");

  /* Envoi de la commande */
  if(write(sfd, cmd, strlen(cmd)+1) == -1) {
    perror("write ");
    return -1;
  }
  if(read(sfd, &ok, sizeof(ok)) == -1) {
    perror("read ");
    return -1;
  }

  buf[TAILLEBUF-1] = '\0';

  /* Lecture du nombre de chars du premier fichier */
  if(read(sfd, &len, sizeof(len)) == -1) {
    perror("read ");
    return -1;
  }
  while(len != -1) {
    /* Vérification pas trop grand */
    if(len > TAILLEBUF-1) {
      printf("Erreur : nom trop long\n");
      return -1;
    }
    /* Lecture du nom de fichier */
    if(read(sfd, buf, len+1) == -1) {
      perror("read ");
      return -1;
    }
    /* Ajout du \0 au cas où */
    buf[len] = '\0';
    printf("%s\n", buf);

    /* Lecture du nombre de chars du fichier suivant */
    if(read(sfd, &len, sizeof(len)) == -1) {
      perror("read ");
      return -1;
    }
  }

  printf("\t\t\t\tFin list\n");
  return 0;
}

int envoiCommandes(int sfd) {
  int nbLus, sortie = 0;
  char message[TAILLEBUF];

  printf("\t\tAppel envoiCommandes\n");

  /* sécurité */
  message[TAILLEBUF - 1] = '\0';

  do {
    if ((nbLus = read(STDIN_FILENO, message, TAILLEBUF-1)) == -1) {
      perror("read");
      exit(1);
    }
    if(nbLus == TAILLEBUF-1) {
      printf("ERREUR commande trop longue\n");
      continue;
    }

    /* Supprimer le \n */
    message[nbLus-1] = '\0';

    if(strncmp(message, "UPLOAD ", 7) == 0) {
      if(upload(sfd, message+7) == -1) {
	printf("Échec upload\n");
	return -1;
      }
    } else if(strncmp(message, "DOWNLOAD ", 9) == 0) {
      if(download(sfd, message+9) == -1) {
	printf("Échec download\n");
	return -1;
      }
    } else if(strcmp(message, "LIST") == 0) {
      if(list(sfd) == -1) {
	printf("Échec list\n");
	return -1;
      }
    } else if(strcmp(message, "EXIT") == 0) {
      sortie = 1;
    }  else {
      printf("Erreur commande\n");
    }

  } while(!sortie);

  printf("\t\tFin envoiCommandes\n");
  return 0;
}

/* 
 * arg1 @serveur 
 * arg2 port serveur 
 */
int main(int argc, char **argv) {
  int sfd;
  struct sockaddr_in addr;
  struct addrinfo hints;
  struct addrinfo *servAddr;

  if(argc != 3) {
    printf("Usage : %s @serveur port\n", argv[0]);
    exit(1);
  }

  printf("Commandes :\n");
  printf("LIST -- lister les fichiers sur le serveur\n");
  printf("UPLOAD nomSurServeur nomSurDisque -- uploader un fichier\n");
  printf("DOWNLOAD nomSurServeur nomSurDisque -- downloader un fichier\n");
  printf("EXIT -- terminer le programme\n\n\n");

  printf("Attention !! Télécharger un fichier vers un du même nom supprimera ce sernier fichier\n\n\n");

  printf("Connexion\n");

  /* Ouverture socket */
  if((sfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }

  /* Recherche IP receveur */
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = 0;
  hints.ai_flags = AI_ADDRCONFIG | AI_CANONNAME;
  if(getaddrinfo(argv[1], NULL, &hints, &servAddr) != 0) {
    perror("getaddrinfo ");
    exit(1);
  }

  /* Données connexion */
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(atoi(argv[2]));
  addr.sin_addr.s_addr = ((struct sockaddr_in *)servAddr->ai_addr)->sin_addr.s_addr;
  freeaddrinfo(servAddr);

  /* Connexion */
  if(connect(sfd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) == -1) {
    perror("connect ");
    exit(1);
  }

  printf("Fin connexion\n");
  envoiCommandes(sfd);

  if(close(sfd) == -1) {
    perror("close ");
    exit(1);
  }

  return EXIT_SUCCESS;
}


