#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <dirent.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <pthread.h>

#define TAILLEBUF 1024
#define NBCLIENTS 5

/* Si un appel de fonction échoue les fonctions upload, download, list
   et traiterClient retournent -1, la fonction appelante, en testant
   le code de retour renvoie aussi -1 => Un échec termine
   traiterClient et ferme la socket. Le client s'appercevra donc de
   l'erreur (sans avoir d'explications)  */

/* Seuls les appels échoués de l'initialisation de la connexion du
   serveur terminent le programme. Les autres terminent seulement la
   communication avec le client */

/* Choix : */
/* Ici, pour l'upload (s'applique aussi au download) l'utilisateur
   peut choisir de donner un nom différent au fichier sur le serveur
   que celui sur son disque dur. Si le nom est le même, il doit entrer
   deux fois le même nom. Ce comportement ne correspond pas vraiment à
   celui d'un serveur FTP */

/* Un seul path donc pas besoin de le mettre dans la structure */
char *path;

struct connexionClient {
  int sfd;
  struct sockaddr_in addr;
  int lenAddr;
};

int upload(int sfd, char *path, char *nom) {
  int lenFic, retourRead, ffd, cpt, retourLock;
  char buf[TAILLEBUF], *cible = NULL;
  struct flock lock;
  
  printf("\t\t\t\tAppel upload\n");

  if((cible = (char*)malloc(sizeof(char)*( strlen(path)+strlen(nom)+2 ))) == NULL) {
    perror("malloc ");
    return -1;
  }

  if(sprintf(cible, "%s/%s", path, nom) < 0) {
    perror("sprintf ");
    return -1;
  }

  /* Ouverture fichier */
  if((ffd = open(cible, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1) {
    perror("open ");
    return -1;
  }

  lock.l_start = 0;
  lock.l_len = 0;
  /* lock.l_pid = getpid();	/\* = 0 ?? *\/ */
  lock.l_pid = pthread_self();	/* = 0 ?? */
  lock.l_type = F_WRLCK;
  lock.l_whence = SEEK_SET;
  if(fcntl(ffd, F_SETLKW, &lock) == -1) {
    perror("fcntl ");
    return -1;
  }
  /* while((retourLock = fcntl(ffd, F_SETLKW, &lock)) < 0) { */
  /*   if(retourLock == -1) { */
  /*     perror("fcntl "); */
  /*     return -1; */
  /*   } */
  /* } */

  /* printf("entré\n"); */

  /* lecture taille fichier */
  if(read(sfd, &lenFic, sizeof(lenFic)) == -1) {
    perror("read ");
    return -1;
  }
  
  cpt = lenFic;
  /* Reception fichier et écriture */
  while(cpt > 0) {
    if((retourRead = read(sfd, buf, TAILLEBUF)) == -1) {
      perror("read ");
      return -1;
    }
    cpt -= retourRead;
    if(write(ffd, buf, retourRead) == -1) {
      perror("write ");
      return -1;
    }
  }

  /* printf("Attente avant libération\n"); */
  /* sleep(3); */
  /* printf("Libération\n"); */

  lock.l_type = F_UNLCK;
  if(fcntl(ffd, F_SETLK, &lock) == -1) {
    perror("lock ");
    return -1;
  }

  free(cible);

  /* Fermeture fichier */
  if(close(ffd) == -1) {
    perror("close ");
    /* return -1; */
  }

  printf("\t\t\t\tFin upload\n");

  return 0;
}

int download(int sfd, char *path, char *nom) {
  int ffd, cpt = 0, nbEnv, retourLock;
  char *contenu, *cible = NULL;
  struct stat stats;
  struct flock lock;
  
  printf("\t\t\t\tAppel download\n");

  if((cible = (char*)malloc(sizeof(char)*( strlen(path)+strlen(nom)+2 ))) == NULL) {
    perror("malloc ");
    return -1;
  }

  if(sprintf(cible, "%s/%s", path, nom) < 0) {
    perror("sprintf ");
    return -1;
  }

  /* Appel de fstat après l'ouverture ?! */
  if(stat(cible, &stats) == -1) {
    perror("stat ");
    return -1;
  }

  if((ffd = open(cible, O_RDONLY)) == -1) {
    perror("open ");
    return -1;
  }

  lock.l_start = 0;
  lock.l_len = 0;
  lock.l_pid = getpid();	/* = 0 ?? */
  lock.l_type = F_RDLCK;
  lock.l_whence = SEEK_SET;

  if(fcntl(ffd, F_SETLKW, &lock) == -1) {
    perror("fcntl ");
    return -1;
  }
  /* while((retourLock = fcntl(ffd, F_SETLKW, &lock)) < 0) { */
  /*   if(retourLock == -1) { */
  /*     perror("fcntl "); */
  /*     return -1; */
  /*   } */
  /* } */

  /* printf("entré\n"); */

  if((contenu = mmap(NULL, stats.st_size, PROT_READ, MAP_PRIVATE, ffd, 0)) == MAP_FAILED) {
    perror("mmap ");
    return -1;
  }

  /* Envoi de la taille du fichier */
  if(write(sfd, &(stats.st_size), sizeof(stats.st_size)) == -1) {
    perror("write");
    return -1;
  }

  /* Envoi du fichier */
  while(cpt != stats.st_size) {
    if((nbEnv = write(sfd, contenu+cpt, stats.st_size - cpt)) == -1) {
      perror("write");
      return -1;
    }
    cpt += nbEnv;
  }

  /* printf("Attente avant libération\n"); */
  /* sleep(3); */
  /* printf("Libération\n"); */

  lock.l_type = F_UNLCK;
  if(fcntl(ffd, F_SETLKW, &lock) == -1) {
    perror("lock ");
    return -1;
  }

  free(cible);

  /* fermeture fichier */
  if(munmap(contenu, stats.st_size) == -1) {
    perror("munmap ");
    /* exit(1); */
  }
  if(close(ffd) == -1) {
    perror("close ");
    /* exit(1); */
  }

  printf("\t\t\t\tFin download\n");
  return 0;
}

int list(int sfd, char *path) {
  int len;

  /* Variables si dossier */
  DIR *pt_Dir;
  struct dirent* dirEnt;

  printf("\t\t\t\tAppel list\n");

  /* Ouverture du dossier */
  if ( ( pt_Dir = opendir (path) ) == NULL) {
    perror ("erreur opendir \n");
    return -1;
  }

  /* Envoi des noms de fichiers */
  while ((dirEnt= readdir (pt_Dir)) !=NULL) {
    /* Ne pas traiter . et .. */
    if(strcmp(dirEnt->d_name, ".") == 0 || strcmp(dirEnt->d_name, "..") == 0) {
      continue;
    }
    /* Envoyer le nom */
    len = strlen(dirEnt->d_name);
    if(write(sfd, &len, sizeof(len)) == -1) {
      perror("write ");
      return -1;
    }
    if(write(sfd, dirEnt->d_name, strlen(dirEnt->d_name) + 1) == -1) {
      perror("write ");
      return -1;
    }
  }

  /* Envoi de -1 -- fin */
  len = -1;
  if(write(sfd, &len, sizeof(len)) == -1) {
    perror("write ");
    return -1;
  }

  /* Fermeture du fichier */
  if(closedir (pt_Dir) == -1) {
    perror("closedir ");
    /* exit(1); */
  }

  printf("\t\t\t\tFin list\n");
  return 0;
}

int traiterClient(int sfd, struct sockaddr_in addr) {
  char buf[TAILLEBUF];
  int nbLus, ok = 1;
  char *p = NULL;

  printf("\t\tAppel traiterClient\n");

  /* sécurité */
  buf[TAILLEBUF - 1] = '\0';

  do {
      
    /* Lecture de la commande */
    if((nbLus = read(sfd, buf, TAILLEBUF-1)) == -1) {
      perror("read ");
      return -1;
    }

    if(write(sfd, &ok, sizeof(ok)) == -1) {
      perror("write ");
      return -1;
    }

    /* printf("nbLus = %d\n%s\n%d\n", nbLus, buf, strlen(buf)); */
    /* printf("int %d\n", buf[strlen(buf)+1]); */

    /* Vérification nb chars reçus */
    if(nbLus == TAILLEBUF - 1) {
      printf("commande trop longue\n");
      return -1;
    } else if(nbLus == 0) {
      printf("Communication terminée\n");
      return 0;
    }

    /* Switch sur commande */
    if(strncmp(buf, "UPLOAD ", 7) == 0) {
      /* Si arborescence précisée (accès à un autre dossier que celui dédié) */
      /* récupérer seulement la dernière partie */
      p = strrchr(buf, '/');
      /* Sinon, se positionner sur l'espace */
      if(p == NULL) p = buf+6;
      /* Incrémenter p pour pointer sur la 1ère lettre */
      p++;

      if(upload(sfd, path, p) == -1) {
	return -1;
      }
    } else if(strncmp(buf, "DOWNLOAD ", 9) == 0) {
      /* Si arborescence précisée (accès à un autre dossier que celui dédié) */
      /* récupérer seulement la dernière partie */
      p = strrchr(buf, '/');
      /* Sinon, se positionner sur l'espace */
      if(p == NULL) p = buf+8;
      /* Incrémenter p pour pointer sur la 1ère lettre */
      p++;

      if(download(sfd, path, p) == -1) {
	return -1;
      }
    } else if(strcmp(buf, "LIST") == 0) {
      if(list(sfd, path) == -1) {
	return -1;
      }
    } else {
      return -1;
    }

  } while (1);

  printf("\t\tFin traiterClient\n");
  return 0;
}

void *appelTraiterClient(void *arg) {
  struct connexionClient *conn = (struct connexionClient *)arg;

  traiterClient(conn->sfd, conn->addr);

  if(close(conn->sfd) == -1) {
    perror("close ");
  }
  printf("Fin traitement client\n");

  free(arg);

  pthread_exit((void*)0);
  return NULL;
}

/* 
 * arg1 : port
 * arg2 : dirPath
 */
int main(int argc, char **argv) {
  struct sockaddr_in addrServ;
  struct connexionClient *conn;
  /* pthread_t tid[NBCLIENTS]; */
  pthread_t tid;
  int sockServ, reuse = 1;

  if(argc != 3) {
    printf("Usage : %s port dirPath\n", argv[0]);
    exit(1);
  }

  path = argv[2];

  /* Retrait du / à la fin du path s'il existe */
  /* A l'air de marcher sans */
  if(argv[2][strlen(argv[2])-1] == '/')
    argv[2][strlen(argv[2])-1] = '\0';

  printf("Connexion\n");

  if((sockServ = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }

  if(setsockopt(sockServ, IPPROTO_IP, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1) {
    perror("setsockopt ");
    exit(1);
  }

  memset(&addrServ, 0, sizeof(addrServ));
  addrServ.sin_family = AF_INET;
  addrServ.sin_port = htons(atoi(argv[1]));
  addrServ.sin_addr.s_addr = htonl(INADDR_ANY);

  if(bind(sockServ, (struct sockaddr *)&addrServ, sizeof(addrServ)) == -1) {
    perror("bind ");
    exit(1);
  }

  if(listen(sockServ, NBCLIENTS) == -1) {
    perror("listen ");
    exit(1);
  }

  printf("Connexion réussie\n");

  for(;;) {
    printf("Attente client\n");

    conn = (struct connexionClient *)malloc(sizeof(struct connexionClient));
    conn->lenAddr = sizeof(conn->addr);

    if((conn->sfd = accept(sockServ, (struct sockaddr *)&(conn->addr), &(conn->lenAddr))) == -1) {
      perror("accept ");
      exit(1);
    }

    printf("Traitement client\n");

    /* Création thread */
    pthread_create(&tid, NULL, appelTraiterClient, (void *)conn);
  }
  

  return EXIT_SUCCESS;
}
