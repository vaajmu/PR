#define _XOPEN_SOURCE 700


#include <netinet/in.h>
#include <arpa/inet.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#include<sys/types.h>
#include<netdb.h>
#include<sys/time.h>
#include<sys/select.h>
#include<pthread.h>
#include<string.h>



#define PING 10

#define PORT 9999
#define PONG 10
#define TAILLEBUF 512


int  waitAnswer(int sock){

  int fromlen;
  char ping[PING];
   struct sockaddr_in exp;


  printf("attente de reception du ping \n");

  fromlen=sizeof(exp);

  do{
   
    if(recvfrom(sock, ping, PING+1, 0, (struct sockaddr *)&exp, &fromlen)==-1){
      perror(" recvfrom");
      exit(1);
    }

      if(sendto(sock,"pong\0",PONG,0,(struct sockaddr *)&exp,sizeof(exp))==-1){
	perror("sendto");
	exit(1);
      }
      
      printf("réponse pong\n");

    

  }while(1);
  
  return 0;
}


int main(int argc, char ** argv){
  
  int sock,yes=1;
  struct sockaddr_in addrCli;

  if(argc!=1){
    printf("usage: %s \n",argv[0]);
    exit(1);
  }

  if((sock=socket(AF_INET,SOCK_DGRAM,0))==-1){
    perror("socket");
    exit(1);
  }

  
  if(setsockopt(sock,SOL_SOCKET, SO_REUSEADDR, &yes , sizeof(yes))==-1){
    perror("setsockopt IPPROTO_IP");
    exit(1);
  }


  memset((void*)&addrCli,0,sizeof(struct sockaddr_in));
  addrCli.sin_family=AF_INET;
  addrCli.sin_port=htons(PORT);
  addrCli.sin_addr.s_addr=htonl(INADDR_ANY);

  if(bind(sock,(struct sockaddr *)&addrCli,sizeof(addrCli))==-1){
    perror("bind");
    exit(1);
  }


  waitAnswer(sock);

  printf("fin de la communication\n");

  if(close(sock)==-1){
    perror("close");
    exit(1);
  }

  return EXIT_SUCCESS;
}
