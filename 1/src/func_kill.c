#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void doNothing(int sig){}

int main (int argc, char *argv[]) {
	
  /* pid_t  pid_fils; */
	
  pid_t *tabPid=NULL;
	
  int i=0, n=0, j;

  sigset_t sigset;
  struct sigaction action;

  sigfillset(&sigset);
  sigdelset(&sigset, SIGUSR1);
	
  sigprocmask(SIG_BLOCK, &sigset, NULL);
	
  action.sa_handler = doNothing;
  action.sa_mask = sigset;
  action.sa_flags = 0;
  sigaction(SIGUSR1, &action, NULL);

  if(argc!= 2) {
    printf("Erreur d'arguments \n");
		
    return EXIT_FAILURE;
		
  }
	
  n=atoi(argv[1]);
	
  tabPid = (pid_t*) malloc( n*sizeof(pid_t));
	
  tabPid[i]=getpid();
	
  while(i<n){
    tabPid[i+1]=fork();
    if(tabPid[i+1]==0) {
      i++;
      tabPid[i]=getpid();		
		
    }
    else if (tabPid[i] == -1) {
      perror("création du fils a échoué..\n");
      return EXIT_FAILURE;
    }
    else {
      printf("%d : mon processus %d \n",i, tabPid[i]);
      printf("%d : le pid de mon fils %d \n",i,tabPid[i+1] );
      if (i==0){
	printf("%d : le pid de mon père est %d \n",i, getppid());
      }
      else printf("%d : le pid de mon père est %d \n", i, tabPid[i-1]);

	
		
      sigfillset(&sigset);
      sigprocmask(SIG_UNBLOCK, &sigset, NULL);
      //sigsuspend(&sigset);

      kill(tabPid[i-1], SIGUSR1);
      return EXIT_SUCCESS;
    }
   
  }
	
  if (i==(n)) {
    printf("\nle dernier processus:\n");
    for(j=0; j<n; j++){
      printf("le pid du processus %d est %d \n", j, tabPid[j]);
    }
  }
  
  kill(tabPid[i-1], SIGUSR1);
  
  return EXIT_SUCCESS;
}
	
