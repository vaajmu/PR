#include <stdio.h>
#include <stdlib.h>
#include "max_func.h"

/* Programme max_calcul */
int main(int argc, char *argv[]) {
  int *tab = NULL;
  int i = 0, max = 0;

  tab = (int *)malloc(argc * sizeof(int));
  if(tab == NULL) {
    fprintf(stderr, "Erreur allocation\n");
  }

  for(i=0; i<argc; i++) {
    tab[i] = atoi(argv[i]);
  }
  
  max = max_func(tab, argc);

  printf("Le maximum est %d\n", max);

  return 0;
}
