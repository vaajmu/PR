/* max_func.c */

int max_func(int tab[], int n) {
  int max = 0, i = 0;

  for (i=0; i<n; i++) {
    if(tab[i] > max) max = tab[i];
  }
  return max;
}

