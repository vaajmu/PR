#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int nfork (int nb_fils) {
  pid_t pid;
  int i,n,fils = 0;
  n= nb_fils;

  for(i=0; i<n;i++){
    pid = fork();    
    if(pid == 0) {
      /* code du fils */
      return 0;
    } else {
      /* code du père */
      if(pid == -1) 
	break;
      fils++;
    }
  }

  if (fils==0) return -1;
  else
    return fils;
}


      /* if (pid == -1 && fils != 0){ */
      /* 	return fils; */
      
      /* } else if(pid == -1 && fils == 0) { */
      /* 	return -1; */
      /* } */


/* int main (int arg, char* argv []) { */
/*   int p; */
/*   int i=1; int N = 3; */
/*   do { */
/*     p = nfork (i) ; */
/*     if (p != 0 ) */
/*       printf ("%d \n",p);  */
/*   } while ((p ==0) && (++i<=N)); */
/*   printf ("FIN \n"); */
/*   return EXIT_SUCCESS; */
/* } */


int main(int argc, char *argv[]) {
  int nbFilsCrees = 0;

  if(argc == 1) {
    fprintf(stderr, "Erreur argument manquant\n");
    return EXIT_FAILURE;
  } else {
    nbFilsCrees = nfork(atoi(argv[1]));
  }

  printf("Retour de nfork : %d\n", nbFilsCrees);

  return EXIT_SUCCESS;
}
