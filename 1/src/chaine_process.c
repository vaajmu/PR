#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

int main (int argc, char *argv[]) {
	
  pid_t  pid_fils;
	
  pid_t *tabPid=NULL;
	
  int status, i=0, val, n=0, j;
	
  if(argc!= 2) {
    printf("Erreur d'arguments \n");
		
    return EXIT_FAILURE;
		
  }
	
  n=atoi(argv[1]);
	
  tabPid = (pid_t*) malloc( n*sizeof(pid_t));
	
  tabPid[i]=getpid();
	
  while(i<n){
    tabPid[i+1]=fork();
    if(tabPid[i+1]==0) {
      i++;
      tabPid[i]=getpid();		
		
    }
    else if (tabPid[i] == -1) {
      perror("création du fils a échoué..\n");
      return EXIT_FAILURE;
    }
    else {
      pid_fils=wait(&status);
      if (WIFEXITED (status)) {
		  val=WEXITSTATUS(status);
      } else 
      {
	  val=-1;
	  }
      printf("%d : mon processus %d \n",i, tabPid[i]);
      printf("%d : le pid de mon fils %d \n",i,pid_fils );
      if (i==0){
		    printf("la valeur aléatoire est: %d \n", val);
	  printf("%d : le pid de mon père est %d \n",i, getppid());
  }
      else printf("%d : le pid de mon père est %d \n", i, tabPid[i-1]);
    
      return val;
    }
   
  }
	
	//printf("valeur de i %d",i);
  if (i==(n)) {
    printf("\nle dernier processus:\n");
    for(j=0; j<n; j++){
      printf("le pid du processus %d est %d \n", j, tabPid[j]);
    }
  }
	srand(time(NULL));
	val=(int) (rand() / (((double) RAND_MAX +1) /100));
  
  return val;
}
	
