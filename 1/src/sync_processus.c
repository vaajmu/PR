#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

void sigRecu(int sig) {
}

int main() {
  pid_t pidP2, pidP3, pidP1;
  sigset_t sigUsr1, sigUsr2, sigVide, sigPlein;
  struct sigaction actionUsr1, actionUsr2;

  /* On initialise le pid du premier processus */
  pidP1 = getpid();

  /* On initialise les masques de signaux */
  sigemptyset(&sigUsr1);
  sigaddset(&sigUsr1, SIGUSR1);

  sigemptyset(&sigUsr2);
  sigaddset(&sigUsr2, SIGUSR2);

  sigemptyset(&sigVide);

  /* sigfillset(&sigPlein); */
  sigemptyset(&sigPlein);
  sigaddset(&sigPlein, SIGUSR1);
  sigaddset(&sigPlein, SIGUSR2);

  /* On masque les signaux USR1 et USR2 pour ne pas être interrompu
     avant de le vouloir */
  /* sigprocmask(SIG_BLOCK, &sigUsr1, NULL); */
  /* sigprocmask(SIG_BLOCK, &sigUsr2, NULL); */
  sigprocmask(SIG_SETMASK, &sigPlein, NULL);

  /* On définit les handlers */
  actionUsr1.sa_handler = sigRecu;
  /* actionUsr1.sa_mask = sigUsr1; */
  actionUsr1.sa_mask = sigVide;
  actionUsr1.sa_flags = 0;
  sigaction(SIGUSR1, &actionUsr1, NULL);
  
  actionUsr2.sa_handler = sigRecu;
  /* actionUsr2.sa_mask = sigUsr2; */
  actionUsr2.sa_mask = sigVide;
  actionUsr2.sa_flags = 0;
  sigaction(SIGUSR2, &actionUsr2, NULL);

  /* On créé les 2 autres processus */
  pidP2 = fork();
  if(pidP2 == -1) {
    perror("erreur fork père\n");
  } else if (pidP2 == 0) {	/* Code fils */
    pidP3 = fork();
    if(pidP3 == -1) {
      perror("erreur fork fils\n");
    } else if(pidP3 == 0) {	/* Code petit fils */
      kill(pidP1, SIGUSR1);	/* Envoi du signal à P1 */
      sleep(2);
    } else {			/* Code fils */
      sleep(2);
      wait(NULL);			/* Attente du petit fils */
      sleep(2);
      kill(pidP1, SIGUSR2);	/* Envoi du signal à P2 */
    }

  } else {			/* Code père */
    sleep(1);
    sigsuspend(&sigUsr1);	/* Attente/traitement du premier signal */
    printf("Processus P3 créé\n");
    sigsuspend(&sigUsr2);	/* Attente/traitement du second signal */
    printf("Processus P3 terminé\n");
    wait(NULL);			/* Attente du fils */
    printf("Processus P2 terminé\n");
  }

  return EXIT_SUCCESS;
}

