#include <setjmp.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

jmp_buf buff,bufg, bufh;
 
void f()
{
	int i=0;
	while(1)
	{
		printf("EXECUTE f() : %d \n",i++);
		sleep(1);
		if(setjmp(buff)==0) longjmp(bufg,1);
	}
}
void g()
{
	int j=1000;
	while(1)
	{
		printf("EXECUTE g() : %d \n",j++);
		sleep(1);
		if(setjmp(bufg)==0) longjmp(bufg,1);
	}
}


void main()
{
	if(setjmp(bufg)==0) f();
	else g();
	
}
