#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>


#define PORT 48310
#define NBVALEURS 5

int main() {
  int sfd;
  struct sockaddr_in sNom;
  
  int i, somme = 0, message;

  if((sfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }

  memset(&sNom, 0, sizeof(sNom));
  sNom.sin_family = AF_INET;
  sNom.sin_port = htons(PORT);
  sNom.sin_addr.s_addr = htonl(INADDR_ANY);

  if(bind(sfd, (struct sockaddr *)&sNom, sizeof(sNom)) == -1) {
    perror("bind ");
    exit(1);
  }

  for(i=0; i<NBVALEURS; i++) {
    if(recvfrom(sfd, &message, sizeof(message), 0, NULL, NULL) == -1) {
      perror("recvfrom ");
      exit(1);
    }

    printf("Réception de %d\n", message);
    somme += message;
  }

  printf("Somme : %d\n", somme);
  close(sfd);
  return EXIT_SUCCESS;
}

