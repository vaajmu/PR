#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/mman.h>
#include <netdb.h>
#include <arpa/inet.h>

/* 
 * arg1 @serveur 
 * arg2 port serveur 
 * arg3 chemin fichier
 */
int main(int argc, char **argv) {
  int sfd;
  struct sockaddr_in addr;
  struct addrinfo hints;
  struct addrinfo *servAddr;

  struct stat stats;
  char *basename = NULL, *contenu = NULL;
  int lenNomFichier = 0, ffd;

  if(argc != 4) {
    printf("Usage : %s @serveur port fichier\n", argv[0]);
    exit(1);
  }

  printf("Initialisation\n");

  /* Isolation du nom du fichier */
  basename = strrchr(argv[3], '/');
  if(basename == NULL) basename = argv[3];
  else basename++;		/* caractère après le / */
  lenNomFichier = strlen(basename);

  /* Ouverture et mapping du fichier */
  printf("Statistiques de %s -- %s\n", argv[3], basename);
  if(stat(argv[3], &stats) == -1) {
    perror("stat ");
    exit(1);
  }

  if((ffd = open(argv[3], O_RDONLY)) == -1) {
    perror("open ");
    exit(1);
  }
  if((contenu = mmap(NULL, stats.st_size, PROT_READ, MAP_PRIVATE, ffd, 0)) == MAP_FAILED) {
    perror("mmap ");
    exit(1);
  }
    
  /* Ouverture socket */
  if((sfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }


  printf("IP : %s\n", argv[1]);

  /* Recherche IP receveur */
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = 0;
  hints.ai_flags = AI_ADDRCONFIG | AI_CANONNAME;
  if(getaddrinfo(argv[1], NULL, &hints, &servAddr) != 0) {
    perror("getaddrinfo ");
    exit(1);
  }

  printf("Port : %s\n", argv[2]);

  /* Données connexion */
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(atoi(argv[2]));
  /* memcpy((void*)&((struct sockaddr_in*)servAddr->ai_addr)->sin_addr,(void*)&addr.sin_addr,sizeof(addr)); */
  addr.sin_addr.s_addr = ((struct sockaddr_in *)servAddr->ai_addr)->sin_addr.s_addr;
  freeaddrinfo(servAddr);


  printf("Connexion\n");
  /* Connexion */
  if(connect(sfd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) == -1) {
    perror("connect ");
    exit(1);
  }

  printf("Communication\n");
  /* Envoi de la taille du nom */
  if(write(sfd, &lenNomFichier, sizeof(lenNomFichier)) == -1) {
    perror("write");
    exit(1);
  }

  /* Envoi du nom */
  if(write(sfd, basename, lenNomFichier+1) == -1) {
    perror("write");
    exit(1);
  }

  /* Envoi du fichier */
  if(write(sfd, contenu, stats.st_size) == -1) {
    perror("write");
    exit(1);
  }

  printf("Fin communication\n");

  /* Fermeture socket */
  /* Inutile ?! */
  /* if(shutdown(sfd, 2) == -1) { */
  /*   perror("shutdown "); */
  /*   exit(1); */
  /* } */
  if(close(sfd) == -1) {
    perror("close ");
    exit(1);
  }

  /* fermeture fichier */
  if(munmap(contenu, stats.st_size) == -1) {
    perror("munmap ");
    exit(1);
  }
  if(close(ffd) == -1) {
    perror("close ");
    exit(1);
  }

  return EXIT_SUCCESS;
}

