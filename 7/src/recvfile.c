#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/socket.h>
#include <string.h>
/* #include <sys/resource.h> */
#include <netinet/in.h>

/* #define PORT 48310 */
#define TAILLEBUF 1024

/* Attention le nom du fichier envoyé par le client n'est pas controlé
   pour éviter d'écrire n'importe où */

void transfert(int sfd) {
  int lenNom, retourRead, ffd;
  char *nomFichier = NULL;
  char buf[TAILLEBUF];
  
  /* lecture taille nom fichier */
  if(read(sfd, &lenNom, sizeof(int)) == -1) {
    perror("read ");
    exit(1);
  }
  
  /* Lecture nom fichier */
  if((nomFichier = (char*)malloc(sizeof(char)*lenNom+1)) == NULL) {
    perror("malloc ");
    exit(1);
  }
  if(read(sfd, nomFichier, sizeof(char)*lenNom+1) == -1) {
    perror("read ");
    exit(1);
  }

  /* Ouverture fichier */
  if((ffd = open(nomFichier, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1) {
    perror("open ");
    exit(1);
  }

  /* Reception fichier et écriture */
  while((retourRead = read(sfd, buf, TAILLEBUF)) > 0) {
    if(write(ffd, buf, retourRead) == -1) {
      perror("write ");
      exit(1);
    }
  }
  if(retourRead == -1) {
    perror("read ");
    exit(1);
  }

  /* Fermeture fichier */
  if(close(ffd) == -1) {
    perror("close ");
    exit(1);
  }
  /* Fermeture socket */
  /* Inutile ?! */
  /* if(shutdown(sfd, 2) == -1) { */
  /*   perror("shutdown "); */
  /*   exit(1); */
  /* } */
  if(close(sfd) == -1) {
    perror("close ");
    exit(1);
  }
}



/* 
 * arg1 port serveur 
 */
int main(int argc, char **argv) {
  struct sockaddr_in addrServ, addrCli;
  int socketServ, socketCli, lenAddrCli, reuse = 1;

  if(argc != 2) {
    printf("Usage : %s portServeur\n", argv[0]);
    exit(1);
  }

  printf("Initialisation\n");
  if((socketServ = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }

  if(setsockopt(socketServ, IPPROTO_IP, SO_REUSEADDR, &reuse, sizeof(int)) == -1) {
    perror("setsockopt ");
    exit(1);
  }

  memset(&addrServ, 0, sizeof(addrServ));
  addrServ.sin_family = AF_INET;
  addrServ.sin_port = htons(atoi(argv[1]));
  addrServ.sin_addr.s_addr = htonl(INADDR_ANY);
  if(bind(socketServ, (struct sockaddr *)&addrServ, sizeof(addrServ)) == -1) {
    perror("bind ");
    exit(1);
  }

  if(listen(socketServ, 1) == -1) {
    perror("listen ");
    exit(1);
  }

  /* Communication */

  lenAddrCli = sizeof(addrCli);
  if((socketCli = accept(socketServ, (struct sockaddr *)&addrCli, &lenAddrCli)) == -1) {
    perror("accept ");
    exit(1);
  }


  printf("Communication\n");
  transfert(socketCli);
  printf("Fin communication\n");


  close(socketServ);

  return EXIT_SUCCESS;
}

