#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>


#define PORT 48310
#define TAILLEBUF 1024

int main() {
  int sfd;
  struct sockaddr_in sNom, exp;
  
  int retour = 0, lenExp = 0, lenMessage = 0;
  char buf[1024], vide[] = "";
  char *id = NULL, *val = NULL;

  if((sfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }

  memset(&sNom, 0, sizeof(sNom));
  sNom.sin_family = AF_INET;
  sNom.sin_port = htons(PORT);
  sNom.sin_addr.s_addr = htonl(INADDR_ANY);

  if(bind(sfd, (struct sockaddr *)&sNom, sizeof(sNom)) == -1) {
    perror("bind ");
    exit(1);
  }

  for(;;) {
    lenExp = sizeof(exp);
    
    printf("Attente message\n");

    if(recvfrom(sfd, buf, 1024, 0, (struct sockaddr *)&exp, &lenExp) == -1) {
      perror("recvfrom ");
      exit(1);
    }
    
    printf("Message reçu : --%s--\n", buf);

    if(buf[0] == 'S') {
      /* id = 3è char */
      id = buf + 2;
      /* val = char après 2è ';' */
      val = strrchr(id, ';') + 1; 
      /* Remplacer le 2è ';' par un '\0' */
      *(val-1) = '\0'; 
      /* Affecter la valeur à l'identifiant */
      /* Retour vaut 0 si réussi ou 1 si échec */
      retour = (setenv(id, val, 1) == -1) ? 1 : 0;

      printf("setenv --%s-- --%s-- ==> %d  (1 = échec)\n", id, val, retour);

      /* Envoyer la réponse */
      if(sendto(sfd, &retour, sizeof(retour), 0, (struct sockaddr *)&exp, sizeof(exp)) == -1) {
	perror("sendto ");
	exit(1);
      }

    } else if(buf[0] == 'G') {
      /* id = 3è char */
      id = buf + 2;
      /* Rechercher la valeur de l'identifiant */
      if((val = getenv(id)) == NULL) {
	val = vide;
      }
      
      if(val != NULL) {
	printf("getenv --%s-- ==> --%s--\n", id, val);
      } else {
	printf("getenv --%s-- ==> NULL\n", id);
      }

      /* Envoyer la réponse */
      if(sendto(sfd, val, strlen(val) + 1, 0, (struct sockaddr *)&exp, sizeof(exp)) == -1) {
	perror("sendto ");
	exit(1);
      }


    } else {
      printf("erreur format --%s--", buf);
    }

  }

  close(sfd);
  return EXIT_SUCCESS;
}

