#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>

#include <time.h>
#include <netdb.h>
#include <netinet/in.h>


#define PORT 48310
#define NBFORK 5
#define MAXALEA 20

void fils(int sfd) {
  struct sockaddr_in addr;

  int alea = 0;

  srand(getpid());

  /* Remplire addr destination */
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(PORT);
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  /* Générer valeur à envoyer */
  alea = rand() % MAXALEA;
  printf("\t\t\t\t\tEnvoi de %d\n", alea);

  if(sendto(sfd, &alea, sizeof(alea), 0, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror("sendto ");
    exit(1);
  }

  printf("\t\t\t\t\t\t\t\t\t\tFin fils\n");
  /* close(sfd); */
}

int main() {
  int sfd;
  struct sockaddr_in sNom;
  
  pid_t pid;

  int i, somme = 0, message, j;

  if((sfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }

  memset(&sNom, 0, sizeof(sNom));
  sNom.sin_family = AF_INET;
  sNom.sin_port = htons(PORT);
  sNom.sin_addr.s_addr = htonl(INADDR_ANY);

  if(bind(sfd, (struct sockaddr *)&sNom, sizeof(sNom)) == -1) {
    perror("bind ");
    exit(1);
  }

  for(j=0; j<NBFORK; j++) {
    if((pid = fork()) == 0) {
      fils(sfd);
      return EXIT_SUCCESS;
    } else if(pid == -1) {
      perror("fork ");
      exit(1);
    }
  }

  for(i=0; i<NBFORK; i++) {
    if(recvfrom(sfd, &message, sizeof(message), 0, NULL, NULL) == -1) {
      perror("recvfrom ");
      exit(1);
    }

    printf("Réception de %d\n", message);
    somme += message;
  }

  printf("Somme : %d\n", somme);
  close(sfd);
  return EXIT_SUCCESS;
}

