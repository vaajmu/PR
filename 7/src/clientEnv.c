#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <time.h>
#include <netdb.h>
#include <netinet/in.h>

#define PORT 48310
#define TAILLEBUF 1024

int main(int argc, char **argv) {
  int sfd;
  struct sockaddr_in addr;
  struct addrinfo hints;
  struct addrinfo *res;
  char message[TAILLEBUF], retour[TAILLEBUF];
  int nbLus = 0;

  if(argc != 2) {
    printf("Usage : %s @serveur\n", argv[0]);
    exit(1);
  }

  /* Remplire addr destination */
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(PORT);

  /* Conversion de l'adr IP de destination */
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_protocol = 0;
  hints.ai_flags = AI_ADDRCONFIG | AI_CANONNAME;
  if(getaddrinfo(argv[1], NULL, &hints, &res) != 0) {
    perror("getaddrinfo ");
    exit(1);
  }

  /* Correct ??? */
  addr.sin_addr.s_addr = ((struct sockaddr_in *)res->ai_addr)->sin_addr.s_addr;
  /* ipDst = res->ai_addr.sin_addr.s_addr; */
  /* memcpy(&((struct sockaddr_in)res->ai_addr.sin_addr), addr.sin_addr) */
  
  freeaddrinfo(res);

  /* Création de la socket */
  if((sfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }

  /* Générer valeur à envoyer */

  do {

    printf("Commande : \n");
    
    if ((nbLus = read(STDIN_FILENO, message, TAILLEBUF-1)) == -1) {
      perror("read");
      exit(1);
    }
    
    /* Supprimer le retour  à la ligne */
    message[nbLus-1] = '\0';
    nbLus--;

    /* Sortir si "exit" */
    if(strcmp(message, "exit") == 0) {
      break;
    }

    message[1] = ';';
    
    /* Retirer les espaces à la fin */
    while(message[nbLus-1] == ' ') {
      message[nbLus-1] = '\0';
      nbLus--;
    }

    if(message[0] == 'S') {
      *(strrchr(message, ' ')) = ';';
    }

    printf("Envoi de --%s--\n", message);
    if(sendto(sfd, message, strlen(message) + 1, 0, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
      perror("sendto ");
      exit(1);
    }

    printf("Attente réception\n");
    if(recvfrom(sfd, retour, TAILLEBUF - 1, 0, NULL, NULL) == -1) {
      perror("recvfrom ");
      exit(1);
    }

    if(message[0] == 'S') {
      if(retour[0] == 0) printf("SUCCESS\n");
      else printf("ECHEC\n");
    } else if(message[0] == 'G') {
      printf("==> %s\n", retour);
    }

  } while(1);			/* Retour par break si message == "exit" */
  
  printf("Fin\n");

  close(sfd);

  return EXIT_SUCCESS;
}


