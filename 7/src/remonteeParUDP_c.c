#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <time.h>
#include <netdb.h>
#include <netinet/in.h>

#define PORT 48310
#define MAXALEA 20

int main(int argc, char **argv) {
  int sfd;
  struct sockaddr_in addr;
  struct addrinfo hints;
  struct addrinfo *res;

  int alea = 0;

  srand(time(NULL));

  if(argc != 2) {
    printf("Usage : %s @IPDst\n", argv[0]);
    exit(1);
  }

  /* Remplire addr destination */
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(PORT);

  /* Conversion de l'adr IP de destination */
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_protocol = 0;
  hints.ai_flags = AI_ADDRCONFIG | AI_CANONNAME;
  if(getaddrinfo(argv[1], NULL, &hints, &res) != 0) {
    perror("getaddrinfo ");
    exit(1);
  }

  /* Correct ??? */
  addr.sin_addr.s_addr = ((struct sockaddr_in *)res->ai_addr)->sin_addr.s_addr;
  /* ipDst = res->ai_addr.sin_addr.s_addr; */
  /* memcpy(&((struct sockaddr_in)res->ai_addr.sin_addr), addr.sin_addr) */
  
  printf("Informations sur le serveur : \n\t@IP : %s\n\tnom : %s\n", 
	 inet_ntoa(((struct sockaddr_in *)res->ai_addr)->sin_addr), res->ai_canonname);
  freeaddrinfo(res);

  /* Création de la socket */
  if((sfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
    perror("socket ");
    exit(1);
  }

  /* Générer valeur à envoyer */
  alea = rand() % MAXALEA;
  printf("Envoi de %d\n", alea);

  if(sendto(sfd, &alea, sizeof(alea), 0, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror("sendto ");
    exit(1);
  }

  close(sfd);
  return EXIT_SUCCESS;
}

