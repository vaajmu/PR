#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>

int main(int argc, char *argv[])
{
  struct addrinfo *result;
  struct addrinfo hints = {};
	
	
  /* Le nom de la machine est passe en argument */
  if (argc != 2) {
    fprintf(stderr, "Usage : %s machine\n", argv[0]);
    exit(1);
  }
	
  /* Remplir la structure dest */
	
  hints.ai_family = AF_INET;
  hints.ai_flags = AI_ADDRCONFIG | AI_CANONNAME;
  hints.ai_protocol = 0;
	
  if (getaddrinfo(argv[1], 0, &hints, &result) != 0) {
    perror("getaddrinfo");
    exit(EXIT_FAILURE);
  }
	
  printf("canon name\t%s\n", result->ai_canonname);
  printf("address\t\t%s\n", inet_ntoa(((struct sockaddr_in*)result->ai_addr)->sin_addr));

  return 0;
}
