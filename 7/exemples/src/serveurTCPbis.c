/** Exemple d'un serveur TCP multi-threads */

#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>


#define PORTSERV 7200

int cpt, sum = 0;


void *process_request(void* param) {
  int sock = (int) param;

  /*** Lire le message ***/
  if (read(sock,&cpt, sizeof(cpt)) < 0) {
    perror("read");
    exit(1);
  }
    
  /*** Traitement du message ***/
  
  sum += cpt;
  printf("sum = %d\n", sum);
  if (write(sock, &sum, sizeof(cpt)) == -1) { 
    perror("write");
    exit(2);
  }
	  
  /* Fermer la connexion */
  shutdown(sock,2);
  close(sock);
  pthread_exit(0);    

  return NULL;
}
	


int main(int argc, char *argv[])
{
  struct sockaddr_in sin;  /* Nom de la socket de connexion */
  struct sockaddr_in exp;  /* Nom de la socket du client */
  int sc ;                 /* Socket de connexion */
  int scom;		      /* Socket de communication */
  int fromlen = sizeof (exp);
  int i;
  pthread_t tid[5];
  
  
  /* creation de la socket */
  if ((sc = socket(AF_INET,SOCK_STREAM,0)) < 0) {
    perror("socket");
    exit(1);
  }
    
  memset((void*)&sin, 0, sizeof(sin));
  sin.sin_addr.s_addr = htonl(INADDR_ANY);
  sin.sin_port = htons(PORTSERV);
  sin.sin_family = AF_INET;

  int reuse = 1;
  setsockopt(sc, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)); 


  /* nommage */
  if (bind(sc,(struct sockaddr *)&sin,sizeof(sin)) < 0) {
    perror("bind");
    exit(2);
  }
  
  listen(sc, 5);

  /* Boucle principale */
	
  for (i = 0;i < 4; i++) {
    if ( (scom = accept(sc, (struct sockaddr *)&exp, &fromlen))== -1) {
      perror("accept");
      exit(2);
    }
    
    /* Creation d'un thread qui traite la requete */
    pthread_create(&tid[i], 0, process_request, (void *)scom);
      
  }

  for (i = 0;i < 4; i++)
    pthread_join(tid[i], 0);

  close(sc);

  return 0;
}
