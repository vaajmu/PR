#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#define NAMESZ 128

int main(int argc, char *argv[])
{
  char host[NAMESZ];
  struct addrinfo *result/* , *ptr */;
  struct addrinfo hints = {};
	
  gethostname(host, NAMESZ);
  printf("host\t\t%s\n", host);
	
  /* Remplir la structure dest */
	
  hints.ai_family = AF_INET;
  hints.ai_flags = AI_ADDRCONFIG | AI_CANONNAME;
  hints.ai_protocol = 0;
	
  if (getaddrinfo(host, 0, &hints, &result) != 0) {
    perror("getaddrinfo");
    exit(EXIT_FAILURE);
  }

  printf("canon name\t%s\n", result->ai_canonname);
	
  printf("address\t\t%s\n", inet_ntoa(((struct sockaddr_in*)result->ai_addr)->sin_addr));

  return (0);
}
