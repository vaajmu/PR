#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>

void rien() {
  printf("passage\n");
}

int main (void) {
  int fd1, fd2, fd3;
  sigset_t masque;
  struct sigaction act;
    
  sigfillset(&masque);
  sigdelset(&masque, SIGINT);

  sigprocmask(SIG_BLOCK, &masque, NULL);

  act.sa_handler = rien;
  act.sa_flags = 0;
  /* act.sa_mask = masque; */
  sigaction(SIGUSR1, &act, NULL);

  if ((fd1 = open ("./fich1", O_RDWR| O_CREAT | O_TRUNC, 0600)) == -1)
    return EXIT_FAILURE;
  if (write (fd1,"abcde", strlen ("abcde")) == -1)
    return EXIT_FAILURE;
  if (fork () == 0) {
    if ((fd2 = open ("./fich1", O_RDWR)) == -1)
      return EXIT_FAILURE;
    printf("123\n");
    if (write (fd1,"123", strlen ("123")) == -1)
      return EXIT_FAILURE;
    kill(getppid(), SIGUSR1);
    printf("45\n");
    if (write (fd2,"45", strlen ("45")) == -1)
      return EXIT_FAILURE;
    close(fd2); 
  } else {
    /* sleep(5); */
    sigdelset(&masque, SIGUSR1);
    sigsuspend(&masque);
    fd3 = dup(fd1);
    if (lseek (fd3,0,SEEK_SET) == -1)
      return EXIT_FAILURE;
    printf("fg\n");
    if (write (fd3,"fg", strlen ("fg")) == -1)
      return EXIT_FAILURE;
    printf("hi\n");
    if (write (fd1,"hi", strlen ("hi")) == -1)
      return EXIT_FAILURE;
    wait (NULL);
    close (fd1);
    close(fd3);
    return EXIT_SUCCESS;
  }
  return 0;
}

