#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>


#define LENBUF 1024

int main(int argc, char *argv[]) {
  struct stat stat;
  int fd1, fd2;
  char *buf;


  /* Ouverture et tests sur les fichiers */
  if(argc != 3) {
    printf("usage : ./mycp src dst\n");
    exit(1);
  }
  if((fd1 = open(argv[1], O_RDONLY)) == -1) {
    perror("open src\n");
    exit(1);
  }
  if((fd2 = open(argv[2], O_WRONLY | O_CREAT | O_EXCL, 0644)) == -1) {
    perror("open dst\n");
    exit(1);
  }
  fstat(fd1, &stat);
  if(!S_ISREG(stat.st_mode)) {
    printf("Fichier non régulier\n");
    exit(1);
  }

  /* Recopie */
  if((buf = mmap(NULL, stat.st_size, PROT_READ, MAP_PRIVATE, fd1, 0)) == MAP_FAILED) {
    perror("erreur map\n");
    exit(1);
  }
  if(write(fd2, buf, stat.st_size) == -1) {
    perror("write\n");
    exit(1);
  }

  /* Fermeture propre */
  if(munmap(buf, stat.st_size) == -1) {
    perror("erreur unmap\n");
    exit(1);
  }

  if(close(fd1) == -1) {
    perror("close\n");
    exit(1);
  }
  if(close(fd2) == -1) {
    perror("close\n");
    exit(1);
  }

  return 0;
}


