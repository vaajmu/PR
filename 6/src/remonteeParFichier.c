#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>


#define MAXALEA 10
#define NPROC 5

void ecrire(int id) {
  int fd, alea;
  srand(getpid());

  alea = rand() % MAXALEA;
  printf("prod %d : aléa = %d\n", id, alea);

  /* écrire aléa dans le fichier */
  if((fd = open("ficTmpRemonteeParFile", O_WRONLY)) == -1) {
    perror("open src\n");
    exit(1);
  }

  /* l'unité d'un offset_t est l'octet :  */
  /* offset = id * taille d'un int en octet */
  /* abstraction de la taille d'un int (32/64 bits ...) */
  lseek(fd, id * sizeof(int), SEEK_SET);
  if(write(fd, &alea, sizeof(int)) == -1) {
    perror("write\n");
    exit(1);
  }
  if(close(fd) == -1) {
    perror("close\n");
    exit(1);
  }
}

int main() {
  int i, alea, fd, somme = 0;
  pid_t pid;

  /* Créer le fichier */
  if((fd = open("ficTmpRemonteeParFile", O_RDWR | O_CREAT | O_EXCL, 0644)) == -1) {
    perror("open src\n");
    exit(1);
  }

  for(i=0; i<NPROC; i++) {
    if((pid = fork()) == -1) {
      perror("fork\n");
      exit(1);
    } else if(pid == 0) {	/* code fils */
      ecrire(i);
      return EXIT_SUCCESS;
    }
  }

  for(i=0; i<NPROC; i++) {
    wait(NULL);
  }

  for(i=0; i<NPROC; i++) {
    if(read(fd, &alea, sizeof(int)) == -1) {
      perror("read\n");
      exit(1);
    }
    somme += alea;
  }

  if(close(fd) == -1) {
    perror("close\n");
    exit(1);
  }
  
  if(unlink("ficTmpRemonteeParFile") == -1) {
    perror("unlink\n");
    exit(1);
  }

  printf("SOMME : %d\n", somme);

  return 0;
}


