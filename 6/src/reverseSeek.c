#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>

int main(int argc, char **argv) {
  int i, fdSrc, fdDst;
  struct stat stats;
  char *nomFichierInv = NULL;
  char c;

  if(argc != 2) {
    printf("usage : %s nomFichier\n", argv[1]);
    exit(1);
  }

  nomFichierInv = (char*)malloc(sizeof(char)*(strlen(argv[1])+5));
  sprintf(nomFichierInv, "%s.inv", argv[1]);

  /* Ouvrir les fichiers */
  if((fdSrc = open(argv[1], O_RDONLY)) == -1) {
    perror("erreur open src\n");
    exit(1);
  }

  if((fdDst = open(nomFichierInv, O_CREAT | O_EXCL | O_WRONLY, 0644)) == -1) {
    perror("erreur open dst\n");
    exit(1);
  }

  /* Connaitre le nombre de chars dans le fichier */
  if(fstat(fdSrc, &stats) == -1) {
    perror("erreur stat\n");
    exit(1);
  }

  /* Lire et écrire */
  for (i=1; i<=stats.st_size; i++) {
    lseek(fdSrc, -i, SEEK_END);

    if(read(fdSrc, &c, sizeof(char)) == -1) {
      perror("erreur read\n");
      exit(1);
    }

    if(write(fdDst, &c, sizeof(char)) == -1) {
      perror("erreur write\n");
      exit(1);
    }
  }

  /* Fermer les fichiers */
  if(close(fdSrc) == -1) {
    perror("erreur close\n");
    exit(1);
  }

  if(close(fdDst) == -1) {
    perror("erreur close\n");
    exit(1);
  }

  return 0;
}


