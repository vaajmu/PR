#define _XOPEN_SOUCE 700

#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>

#define _POSIX_SOUCE 1

int ext_grep(char *expr, char *chemin) {
  /* Variables */
  int nbRes = 0;

  /* Variables du fichier en argument */
  struct stat stats;

  /* Variables si dossier */
  DIR *pt_Dir;
  struct dirent* dirEnt;
  char *nouveauChemin = NULL;
  int lenChemin = 0;

  /* Variables si fichier */
  char *contenu = NULL;
  int fd;

  /* printf("Recherche dans %s\n", chemin); */

  /* Récupération des stats de l'argument */
  if(stat(chemin, &stats) == -1) {
    perror("erreur stat\n");
    exit(1);
  }

  if(S_ISREG(stats.st_mode) && stats.st_size > 0) {	/* Cas terminal : fichier (non vide) */
    if((fd = open(chemin, O_RDONLY)) == -1) {
      perror("erreur open\n");
      exit(1);
    }

    /* 
       Note : erreur sur un fichier de 4.7Go : "Value too large for defined data type" 
       Machine sur 32bits : 4Go adressable
       Solution possible : afficher l'erreur et continuer quand même
     */
    if((contenu = mmap(NULL, stats.st_size, PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED) {
      perror("erreur map\n");
      exit(1);
    }

    if(strstr(contenu, expr) != NULL) {
      printf("Trouvé dans %s\n", chemin);
      nbRes = 1;
    } else {
      nbRes = 0;
    }

    if(munmap(contenu, stats.st_size) == -1) {
      perror("erreur munmap\n");
      exit(1);
    }
    if(close(fd) == -1) {
      perror("erreur close\n");
      exit(1);
    }
    return nbRes;

  } else if(S_ISDIR(stats.st_mode)) { /* Cas récursif : dossier */

    lenChemin = strlen(chemin);

    /* Ouverture du dossier */
    if ( ( pt_Dir = opendir (chemin) ) == NULL) {
      perror ("erreur opendir \n");
      exit (1);
    }

    /* Récursion sur tous les éléments du dossier */
    while ((dirEnt= readdir (pt_Dir)) !=NULL) {
      /* Ne pas traiter . et .. */
      if(strcmp(dirEnt->d_name, ".") == 0 || strcmp(dirEnt->d_name, "..") == 0) {
	continue;
      }

      nouveauChemin = (char*)malloc(sizeof(char)*(lenChemin + strlen(dirEnt->d_name) + 2));
      sprintf(nouveauChemin, "%s/%s", chemin, dirEnt->d_name);
      nbRes += ext_grep(expr, nouveauChemin);
      free(nouveauChemin);
    }

    /* Fermeture du fichier */
    closedir (pt_Dir);
    return nbRes;

  } else {			/* Autre type : pas de recherche */
    return 0;
  }
}





int main (int argc, char* argv []) {
  int nbRes;

  if(argc != 3) {
    printf("usage : ./extended-grep expr chemin");
    exit(1);
  }

  /* Suppression du dernier / si existe */
  if(argv[2][strlen(argv[2])-1] == '/') argv[2][strlen(argv[2])-1] = '\0';

  nbRes = ext_grep(argv[1], argv[2]);

  if(nbRes == 0) {
    printf("Aucun fichier valide\n");
  }

  return 0;
}

