#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>


#define LENBUF 1024
#define NPROC 3

void lire(int fd, int id) {
  int lect;
  char c;
  /* Recopie */
  while((lect = read(fd, &c, sizeof(char))) != 0) {
    printf("%d : %c\n", id, c);
  }
  if(lect == -1) {
    perror("read\n");
    exit(1);
  }
}


int main(int argc, char *argv[]) {
  int i, fd;
  pid_t pid;

  if(argc != 2) {
    printf("usage : ./prgPosix nomFic\n");
    exit(1);
  }

  if((fd = open(argv[1], O_RDONLY)) == -1) {
    perror("open src\n");
    exit(1);
  }

  for(i=0; i<NPROC; i++) {
    if((pid = fork()) == -1) {
      perror("fork\n");
      exit(1);
    } else if(pid == 0) {	/* code fils */
      lire(fd, i);
      return EXIT_SUCCESS;
    }
  }

  for(i=0; i<NPROC; i++) {
    wait(NULL);
  }

  /* Fermer les fichiers */
  if(close(fd) == -1) {
    perror("close\n");
    exit(1);
  }

  return 0;
}


