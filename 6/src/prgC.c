#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>


#define LENBUF 1024
#define NPROC 3

void lire(FILE *f, int id) {
  char c;
  //char buf[1024];

  if(setvbuf(f, NULL, _IONBF, 1024) != 0) {
    perror("setvbuf\n");
    exit(1);
  }
  /* sleep(3); */

  while((c = fgetc(f)) != EOF) {
    printf("%d : %c\n", id, c);
    /* sleep(1); */
  }
  printf("%d fin\n", id);
}


int main(int argc, char *argv[]) {
  int i;
  pid_t pid;
  FILE *f;

  if(argc != 2) {
    printf("usage : ./prgPosix nomFic\n");
    exit(1);
  }

  if((f = fopen(argv[1], "r")) == NULL) {
    perror("open src\n");
    exit(1);
  }

  for(i=0; i<NPROC; i++) {
    if((pid = fork()) == -1) {
      perror("fork\n");
      exit(1);
    } else if(pid == 0) {	/* code fils */
      lire(f, i);
      return EXIT_SUCCESS;
    }
  }

  for(i=0; i<NPROC; i++) {
    wait(NULL);
  }

  /* Fermer les fichiers */
  if(fclose(f) == EOF) {
    perror("close\n");
    exit(1);
  }

  return 0;
}


