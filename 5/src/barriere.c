#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/ipc.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <errno.h>
#include <mqueue.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>


#define NBPROC 10

sem_t *sem, *mutex;
int *sp;

void wait_barrier(int N) {
  int cpt, i;
  
  sem_wait(mutex);
  cpt = ++(*sp);
  sem_post(mutex);

  if(cpt < N) {
    sem_wait(sem);
  } else {
    for(i=0; i<N-1; i++)	/* ne pas libérer le dernier qui n'a
				   pas fait de wait */
      sem_post(sem);
  }

}

int main () {
  
  pid_t pid;
  int fd, i;

  /* création shm */
  if((fd=shm_open("/barriere:1",O_RDWR | O_CREAT, 0666))==-1) {
    perror("shm_open\n");
    exit(1);
  }
  if(ftruncate(fd, sizeof(int))==-1){
    perror("ftruncate");
    exit(1);
  }
  if((sp=mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED,fd,0))==MAP_FAILED){
    perror("mmap\n");
    exit(1);
  }
  *sp = 0;

  /* création mutex */
  if((mutex = sem_open("/barriere:2", O_CREAT | O_EXCL | O_RDWR, 0666, 1)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    mutex = sem_open("/barriere:2", O_RDWR);
  }
  /* création semaphore */
  if((sem = sem_open("/barriere:3", O_CREAT | O_EXCL | O_RDWR, 0666, 0)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    sem = sem_open("/barriere:3", O_RDWR);
  }

  for(i=0;i<NBPROC;i++){
    if((pid=fork())==0){

      printf("%d avant barriere\n",i);
      wait_barrier(NBPROC);
      printf("\t\t\t\t%d après barriere\n",i);
     
      return EXIT_SUCCESS;

    }else if(pid==-1) {
      printf("erreur de fork..\n");
      exit(1);
    }
  }

  for(i=0;i<NBPROC;i++){
    wait(NULL);
  }

  /* Libération shm */
  munmap(sp,sizeof(sp));
  shm_unlink("/barriere:1");
		
  /* Libération sem */
  sem_close(sem);
  sem_unlink("/barriere:3");

  /* Pas d'IPC résiduelles sans ces lignes */
  /* sem_close(mutex); */
  /* sem_unlink("/barriere:2"); */
		
  return (0);
}
