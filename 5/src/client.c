#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <errno.h>
#include <mqueue.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

#define MSGLEN 1024

struct request {
  long type;
  char content[MSGLEN];
};

typedef struct serveur Serveur;
struct serveur {
  char *nom;
  sem_t *reveil;
  sem_t *ecr;
  struct request *msg;
};

typedef struct client Client;
struct client {
  char *nom;
  sem_t *reveil;
  sem_t *ecr;
  struct request *msg;
  Serveur serveur;
};

void structurerPath(char *nom, char **path, int *indice) {
  int lenNom = strlen(nom);

  *indice = lenNom + 6;
  *path = (char*)malloc(sizeof(char)*(lenNom+8));
  sprintf(*path, "/%s_shm:0", nom);
}

void allocationIPCClient(Client *client) {
  int shm, indiceNum;
  char *path;

  structurerPath(client->nom, &path, &indiceNum);

  /* création shm client */
  if((shm = shm_open(path, O_RDWR | O_CREAT, 0666))==-1) {
    perror("shm_open\n");
    exit(1);
  }
  if(ftruncate(shm, sizeof(struct request))==-1){
    perror("ftruncate");
    exit(1);
  }
  if((client->msg = (struct request *)mmap(NULL, sizeof(struct request), PROT_READ | PROT_WRITE, MAP_SHARED, shm, 0)) == MAP_FAILED){
    perror("mmap\n");
    exit(1);
  }

  /* création mutex écriture client */
  path[indiceNum] = '1';
  if((client->ecr = sem_open(path, O_CREAT | O_EXCL | O_RDWR, 0666, 0)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    client->ecr = sem_open(path, O_RDWR);
  }
  /* création mutex réveil serveur */
  path[indiceNum] = '2';
  if((client->reveil = sem_open(path, O_CREAT | O_EXCL | O_RDWR, 0666, 0)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    client->reveil = sem_open(path, O_RDWR);
  }
  free(path);
}

void allocationIPCServeur(Serveur *serveur) {
  char *path = NULL;
  int indiceNum, shm;

  structurerPath(serveur->nom, &path, &indiceNum);
  
  /* récupération shm serveur */
  if((shm = shm_open(path, O_RDWR | O_CREAT, 0666))==-1) {
    perror("shm_open\n");
    exit(1);
  }
  if((serveur->msg = (struct request *)mmap(NULL, sizeof(struct request), PROT_READ | PROT_WRITE, MAP_SHARED, shm, 0)) == MAP_FAILED){
    perror("mmap\n");
    exit(1);
  }

  /* Pas besoin de tester si existantes, elles doivent déjà exister */
  /* récupération mutex écriture serveur */
  path[indiceNum] = '1';
  if((serveur->ecr = sem_open(path, O_RDWR)) == SEM_FAILED) {
    perror("sem_open ecr s (Le serveur est-il démarré)\n");
    exit(1);
  }
  path[indiceNum] = '2';
  /* récupération mutex réveil serveur */
  if((serveur->reveil = sem_open(path, O_RDWR)) == SEM_FAILED) {
    perror("sem_open rev s\n");
    exit(1);
  }
  free(path);
}

void liberationIPCClient(Client *client) {
  char *path;
  int indiceNum;

  structurerPath(client->nom, &path, &indiceNum);

  /* Libération shm */
  munmap(client->msg, sizeof(struct request));
  shm_unlink(path);

  /* Libération sem */
  path[indiceNum] = '1';
  sem_close(client->ecr);
  sem_unlink(path);

  path[indiceNum] = '2';
  sem_close(client->reveil);
  sem_unlink(path);

  free(path);
}

void liberationIPCServeur(Serveur *serveur) {
  char *path;
  int indiceNum;

  structurerPath(serveur->nom, &path, &indiceNum);

  /* Libération shm */
  munmap(serveur->msg, sizeof(struct request));

  /* Libération sem */
  path[indiceNum] = '1';
  sem_close(serveur->ecr);

  path[indiceNum] = '2';
  sem_close(serveur->reveil);

  free(path);
}

int lectureMessagesServeur(Client *client) {
  char chaine[MSGLEN];
  int retour = 0;
  
  while(retour != -1) {
    sem_post(client->ecr);
    sem_wait(client->reveil);
    strcpy(chaine, client->serveur.msg->content);
    printf("MESSAGE REÇU : \"%s\"\n", chaine);
    if(strcmp(chaine, "exit serveur") == 0)
      retour = -1;
  }
  return -1;
}

void envoiMessageServeur(Client *client, int type, char *content) {
  printf("ENVOI MESAGE\n");
  sem_wait(client->serveur.ecr);
  client->serveur.msg->type = type;
  strcpy(client->serveur.msg->content, content);
  sem_post(client->serveur.reveil);
}

int lectureEtEnvoiMessagesClaviers(Client *client) {
  int i;
  int ir = 1;
  char message[MSGLEN];
  char *lastCR = NULL;

  while (ir > 0) 
    {
      for (i = 0; i < MSGLEN ; i++)
	message[i] = '\0';
      if ((ir = read(STDIN_FILENO, message, MSGLEN-1)) == -1)
	perror("read");

      /* Remplacer le \n (validation du clavier) par un \0 */
      lastCR = strrchr(message, '\n');
      *lastCR = '\0';

      envoiMessageServeur(client, 1, message);
    }
  return -1;
}

int main (int argc, char* argv[]) {
  Client client;
  pid_t pidLectClavier = 0, pidLectServ = 0, procTermine = 0;

  if(argc != 3) {
    printf("usage : ./client nom_client nom_serv\n");
    exit(1);
  }

  client.nom = argv[1];
  allocationIPCClient(&client);

  client.serveur.nom = argv[2];
  allocationIPCServeur(&(client.serveur));

  envoiMessageServeur(&client, 0, client.nom);

  /* Créer un fils pour lire les messages du terminal */
  if((pidLectClavier = fork()) == -1) {
    perror("erreur fork\n");
    exit(1);
  } else if(pidLectClavier == 0) {
    /* Attendre et lire les messages du serveur */
    lectureEtEnvoiMessagesClaviers(&client);
    return EXIT_SUCCESS;
  }

  /* Créer un fils pour lire les messages du serveur */
  if((pidLectServ = fork()) == -1) {
    perror("erreur fork\n");
    exit(1);
  } else if(pidLectServ == 0) {
    /* Attendre et lire les messages du serveur */
    lectureMessagesServeur(&client);
    return EXIT_SUCCESS;
  }

  /* Attendre qu'un fils se termine */
  procTermine = wait(NULL); 
  /* tuer son frère */
  if(procTermine == pidLectClavier) {
    kill(pidLectServ, SIGINT);
    /* Fin par Ctrl+D => envoi d'un message de déconnexion */
    envoiMessageServeur(&client, 2, client.nom);
  } else {			/* sinon fin par un message du serveur */
    kill(pidLectClavier, SIGINT);
  }

  liberationIPCServeur(&(client.serveur));
  liberationIPCClient(&client);

  return (0);
}
