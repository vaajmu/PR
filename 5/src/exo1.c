#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/ipc.h>
#include <sys/mman.h>
#include <mqueue.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>


#define NBPROC 10
#define MAXSIZE 50
#define MAXALEA 10

int main () {
  
  int *sp;
  pid_t pid;
  int fd, random_val,somme=0,  i;
 
  

  if((fd=shm_open("/file",O_RDWR | O_CREAT, 0666))==-1) {
    perror("shm_open\n");
    exit(1);
  }

  if(ftruncate(fd, sizeof(int)*NBPROC)==-1){
    perror("ftruncate");
    exit(1);
  }

  if((sp=mmap(NULL, NBPROC*sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED,fd,0))==MAP_FAILED){
    perror("mmap\n");
    exit(1);
  }

  for(i=0;i<NBPROC;i++){
    if((pid=fork())==0){
      srand(getpid());
      random_val = (int) (10*(float)rand()/ RAND_MAX);

      printf("fils: %d :valeur envoyé est : %d \n",i, random_val);
      sp[i]=random_val;
     
      return EXIT_SUCCESS;

    }else if(pid==-1) {
      printf("erreur de fork..\n");
      exit(1);
    }
  }

  for(i=0;i<NBPROC;i++){
    wait(NULL);
  }


  for(i=0;i<NBPROC;i++){
printf("valeur reçue est : %d \n",sp[i]);
  somme+=sp[i];
  }
  
		
  printf("somme = %d \n",somme);
		
		
  munmap(sp,sizeof(sp));

  shm_unlink("/file");
		
  return (0);
}
