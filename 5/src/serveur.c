#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <errno.h>
#include <mqueue.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

#define MAXCLIENTS 10
#define MAXSIZE 1024

struct request {
  long type;
  char content[MAXSIZE];
};

typedef struct client Client;
struct client {
  int connecte;
  char *nom;
  sem_t *reveil;
  sem_t *ecr;
  struct request *msg;
};

typedef struct serveur Serveur;
struct serveur {
  sem_t *reveil;
  sem_t *ecr;
  struct request *msg;
  int cptClients;
  Client clients[MAXCLIENTS];
};

void structurerPath(char *nom, char **path, int *indice) {
  int lenNom = strlen(nom);

  *indice = lenNom + 6;
  *path = (char*)malloc(sizeof(char)*(lenNom+8));
  sprintf(*path, "/%s_shm:0", nom);
}

void allocationIPCServeur(Serveur *serveur, char *nomServeur) {
  char *path = NULL;
  int indiceNum;
  int shm;

  structurerPath(nomServeur, &path, &indiceNum);
  
  /* Libération des semaphores si non libérées par un autre */
  /* Note : devrait être fait par le Makefile */
  printf("Légal ???\n");
  path[indiceNum] = '1';
  sem_unlink(path);
  path[indiceNum] = '2';
  sem_unlink(path);

  /* création shm serveur */
  path[indiceNum] = '0';
  if((shm = shm_open(path, O_RDWR | O_CREAT, 0666))==-1) {
    perror("shm_open\n");
    exit(1);
  }
  if(ftruncate(shm, sizeof(struct request))==-1){
    perror("ftruncate");
    exit(1);
  }
  if((serveur->msg = (struct request *)mmap(NULL, sizeof(struct request), PROT_READ | PROT_WRITE, MAP_SHARED, shm, 0)) == MAP_FAILED){
    perror("mmap\n");
    exit(1);
  }

  /* création mutex écriture serveur */
  path[indiceNum] = '1';
  if((serveur->ecr = sem_open(path, O_CREAT | O_EXCL | O_RDWR, 0666, 0)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    serveur->ecr = sem_open(path, O_RDWR);
  }
  /* création mutex réveil serveur */
  path[indiceNum] = '2';
  if((serveur->reveil = sem_open(path, O_CREAT | O_EXCL | O_RDWR, 0666, 0)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    serveur->reveil = sem_open(path, O_RDWR);
  }
  free(path);
}

void liberationIPCServeur(Serveur *serveur, char *nomServeur) {
  char *path = NULL;
  int indiceNum;

  structurerPath(nomServeur, &path, &indiceNum);
  
  /* Libération shm */
  munmap(serveur->msg, sizeof(struct request));
  shm_unlink(path);

  /* Libération sem */
  path[indiceNum] = '1';
  sem_close(serveur->ecr);
  sem_unlink(path);

  path[indiceNum] = '2';
  sem_close(serveur->reveil);
  sem_unlink(path);
}

void allocationIPCClient(Client *client) {
  char *path = NULL;
  int indiceNum;
  int shm;

  structurerPath(client->nom, &path, &indiceNum);
  
  /* création shm client */
  path[indiceNum] = '0';
  if((shm = shm_open(path, O_RDWR | O_CREAT, 0666))==-1) {
    perror("shm_open\n");
    exit(1);
  }
  if((client->msg = (struct request *)mmap(NULL, sizeof(struct request), PROT_READ | PROT_WRITE, MAP_SHARED, shm, 0)) == MAP_FAILED){
    perror("mmap\n");
    exit(1);
  }

  /* création mutex écriture client */
  path[indiceNum] = '1';
  if((client->ecr = sem_open(path, O_CREAT | O_EXCL | O_RDWR, 0666, 0)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    client->ecr = sem_open(path, O_RDWR);
  }
  /* création mutex réveil serveur */
  path[indiceNum] = '2';
  if((client->reveil = sem_open(path, O_CREAT | O_EXCL | O_RDWR, 0666, 0)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    client->reveil = sem_open(path, O_RDWR);
  }
}

void liberationIPCClient(Client *client) {
  char *path = NULL;
  int indiceNum;

  structurerPath(client->nom, &path, &indiceNum);
  
  /* Libération shm */
  munmap(client->msg, sizeof(struct request));

  /* Libération sem */
  path[indiceNum] = '1';
  sem_close(client->ecr);

  path[indiceNum] = '2';
  sem_close(client->reveil);
}

void libererClient(Client *client) {
  liberationIPCClient(client);
  client->connecte = 0;
  free(client->nom);
}

void retirerClient(Serveur *serveur) {
  int i = 0;

  /* Tant que i < MAXCLIENTS */
  /* boucle si client[i] non connecté */
  /* boucle si client[i].nom != message.nom */
  while((i < MAXCLIENTS) && (serveur->clients[i].connecte == -1 || strcmp(serveur->clients[i].nom, serveur->msg->content) != 0)) {
    i++;
  }
  if(i >= MAXCLIENTS) {
    printf("retirer client : mauvais nom\n");
    return;
  }

  printf("RETRAIT CLIENT %s INDICE %d\n", serveur->msg->content, i);

  libererClient(&(serveur->clients[i]));
  serveur->cptClients--;
}

int envoyerMsg(Serveur *serveur) {
  int i, retour = 0;

  printf("ENVOI MESSAGE %s\n", serveur->msg->content);

  for(i=0; i<MAXCLIENTS; i++) {
    if(serveur->clients[i].connecte == 1) {
      sem_wait(serveur->clients[i].ecr);
      serveur->clients[i].msg->type = serveur->msg->type;
      strcpy(serveur->clients[i].msg->content, serveur->msg->content);
      sem_post(serveur->clients[i].reveil);
    }
  }

  if(strcmp(serveur->msg->content, "exit serveur") == 0) {
    retour = -1;
  }

  return retour;
}

void ajouterClient(Serveur *serveur) {
  int i = 0;

  if(serveur->cptClients >= MAXCLIENTS) {
    printf("ajouter client : Trop de clients, pas ajouté\n");
    return;
  }
  serveur->cptClients++;

  while(serveur->clients[i].connecte == 1) {
    i++;
  }

  printf("AJOUT CLIENT %s INDICE %d\n", serveur->msg->content, i);

  serveur->clients[i].connecte = 1;
  serveur->clients[i].nom = strdup(serveur->msg->content);
  allocationIPCClient(&(serveur->clients[i]));
}

int main (int argc, char* argv[]) {
  char *nomServeur;
  Serveur serveur;
  int i = 0, retour = 0;

  if(argc != 2) {
    printf("usage : ./serveur nom_serveur\n");
    exit(1);
  }

  nomServeur = argv[1];

  /* Initialisation des variables du serveur */
  serveur.cptClients = 0;
  for(i=0; i<MAXCLIENTS; i++) {
    serveur.clients[i].connecte = 0;
  }

  allocationIPCServeur(&serveur, nomServeur);

  /* fonctionnement principal du serveur */
  while(retour != -1) {
    sem_post(serveur.ecr);
    sem_wait(serveur.reveil);

    /* Sécurité */
    serveur.msg->content[MAXSIZE-1] = '\0';

    switch(serveur.msg->type) {
    case 0:
      ajouterClient(&serveur);
      break;
    case 1:
      retour = envoyerMsg(&serveur);
      break;
    case 2:
      retirerClient(&serveur);
      break;
    }
  }

  /* libérer les IPC des clients encore connectés */
  for(i=0; i<MAXCLIENTS; i++) {
    if(serveur.clients[i].connecte == 1) {
      libererClient(&(serveur.clients[i]));
    }
  }

  liberationIPCServeur(&serveur, nomServeur);

  return EXIT_SUCCESS;
}
