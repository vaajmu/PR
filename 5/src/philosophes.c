#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

/* Nombre de philosophes */
#define NPHI 5
/* Nombre de fois que le philosophe vas manger */
#define NBCYCLES 5
/* Nombre maximal de secondes durant lesquelles il pense */
#define MAXTPSPENSER 5
/* Nombre maximal de secondes durant lesquelles il mange */
#define MAXTPSMANGER 5

typedef struct table Table;
struct table {
  int *baguettes;
  int *demandes;
  sem_t *sem[NPHI];
  sem_t *accesTab;
};

/* Affiche l'état des variables */
void affiche(Table table, int id, char *message) {
  int i;

  /* Affiche l'action */
  for(i=0; i<id; i++) {
    printf("|  |");
  }
  printf("|%s|", message);
  for(; i<NPHI-1; i++) {
    printf("|  |");
  }
  printf("\n");

  /* Affiche le tab des baguettes */
  for(i=0; i<NPHI; i++) {
    printf("      ");
  }
  for(i=0; i<NPHI; i++) {
    printf("| %d |", table.baguettes[i]);
  }
  printf("\n");

  /* Affiche le tab des demandes */
  for(i=0; i<NPHI; i++) {
    printf("      ");
  }
  for(i=0; i<NPHI; i++) {
    printf("| %d |", table.demandes[i]);
  }
  printf("\n\n");
}

void penser(int id) {
  int temps = rand()%MAXTPSPENSER;
  sleep(temps);
}

void manger(int id) {
  int temps = rand()%MAXTPSMANGER;
  sleep(temps);
}

void philosophe(int id, Table *table) {
  int bloque;
  int i;
  /* int tmp; */
  /* tmp = id; */

  srand(getpid());
  
  for(i=0; i<NBCYCLES; i++) {
    affiche(*table, id, "dP");
    penser(id);
    affiche(*table, id, "fP");
    sem_wait(table->accesTab);
    do {			/* tant qu'une baguette n'est pas
				   disponible */
      bloque = -1;
      /* incrémenter la demande d'accès à une baguette */
      if(table->baguettes[id] == 0) {
	bloque = id;
	table->demandes[id]++;	/* ou '= 1' puisqu'il ne peut y avoir
				   que 2 philosophes qui demandent
				   cette baguette et qu'un est en
				   train de manger => pas nécéssaire
				   de savoir combien attendent */
      } else if(table->baguettes[(id+1)%NPHI] == 0) {
	bloque = (id+1)%NPHI;
	table->demandes[(id+1)%NPHI]++; /* même remarque */
      }

      if(bloque != -1) {
	/* libérer l'accès au tableau et attendre la libération de la
	   baguette */
	affiche(*table, id, "dB");
	sem_post(table->accesTab);
	sem_wait(table->sem[bloque]);
	/* redemander l'accès au tableau et décrémenter la
	   demande d'accès à une baguette */
	sem_wait(table->accesTab);
	affiche(*table, id, "fB");
	table->demandes[bloque]--;
      }

    }while(bloque != -1);

    /* prendre les baguettes et libérer l'accès au tableau */
    table->baguettes[id] = 0;
    table->baguettes[(id+1)%NPHI] = 0;

    affiche(*table, id, "dM");
    sem_post(table->accesTab);

    manger(id);

    /* redemander l'accès au tableau et reposer les baguettes */
    sem_wait(table->accesTab);
    table->baguettes[id] = 1;
    table->baguettes[(id+1)%NPHI] = 1;
    affiche(*table, id, "fM");
    /* si une baguette manquait à quelqu'un, le réveiller et libérer
       l'accès au tableau */
    if(table->demandes[id] > 0) {
      affiche(*table, id, "S ");
      sem_post(table->sem[id]);
    }
    if(table->demandes[(id+1)%NPHI] > 0) {
      affiche(*table, id, " S");
      sem_post(table->sem[(id+1)%NPHI]);
    }
    sem_post(table->accesTab);
  }

}

/* Construit un chemin pour créer les sémaphores */
void structurerPath(char *id, char **path) {
  int lenNom = strlen(id);
  *path = (char*)malloc(sizeof(char)*(lenNom+17));
  sprintf(*path, "/philosophe%s_shm:0", id);
}

int main() {
  int i, shm;
  pid_t pid;
  Table table;
  char *path;
  char numPhi[3];

  /* Créer shm */
  if((shm = shm_open("philosophes:0", O_RDWR | O_CREAT, 0666)) == -1) {
    perror("shm_open\n");
    exit(1);
  }
  if((ftruncate(shm, sizeof(int)*NPHI)) == -1) {
    perror("ftruncate\n");
    exit(1);
  }
  if((table.baguettes = mmap(NULL, sizeof(int)*NPHI, PROT_READ | PROT_WRITE, MAP_SHARED, shm, 0)) == MAP_FAILED) {
    perror("mmap\n");
    exit(1);
  }

  if((shm = shm_open("philosophes:1", O_RDWR | O_CREAT, 0666)) == -1) {
    perror("shm_open\n");
    exit(1);
  }
  if((ftruncate(shm, sizeof(int)*NPHI)) == -1) {
    perror("ftruncate\n");
    exit(1);
  }
  if((table.demandes = mmap(NULL, sizeof(int)*NPHI, PROT_READ | PROT_WRITE, MAP_SHARED, shm, 0)) == MAP_FAILED) {
    perror("mmap\n");
    exit(1);
  }

  /* Initialisation de table */
  for(i=0; i<NPHI; i++) {
    table.baguettes[i] = 1;
    table.demandes[i] = 0;

    sprintf(numPhi, "%d", i);
    structurerPath(numPhi, &path);

    if((table.sem[i] = sem_open(path, O_CREAT | O_EXCL | O_RDWR, 0666, 0)) == SEM_FAILED) {
      if(errno != EEXIST) {
	perror("sem_open\n");
	exit(1);
      }
      table.sem[i] = sem_open(path, O_RDWR);
    }
    free(path);
  }

  if((table.accesTab = sem_open("philosopheAcces:0", O_CREAT | O_EXCL | O_RDWR, 0666, 1)) == SEM_FAILED) {
    if(errno != EEXIST) {
      perror("sem_open\n");
      exit(1);
    }
    table.accesTab = sem_open("philosopheAcces:0", O_RDWR);
  }



  /* créer les philosophes */
  for(i=0; i<NPHI; i++) {
    if((pid = fork()) == -1) {
      perror("erreur fork\n");
      exit(1);
    } else if (pid == 0) {	/* Code du fils */
      philosophe(i, &table);
      return EXIT_SUCCESS;
    }
  }

  /* attendre la terminaison des philosophes */
  for(i=0; i<NPHI; i++) {
    wait(NULL);
  }

  /* Libération des sémaphores */
  for(i=0; i<NPHI; i++) {
    sprintf(numPhi, "%d", i);
    structurerPath(numPhi, &path);
    sem_close(table.sem[i]);
    sem_unlink(path);
    free(path);
  }
  sem_close(table.accesTab);
  sem_unlink("philosopheAcces:0");

  /* Libération des shm */
  munmap(table.baguettes, sizeof(int)*NPHI);
  shm_unlink("philosophes:0");

  munmap(table.demandes, sizeof(int)*NPHI);
  shm_unlink("philosophes:1");

  return EXIT_SUCCESS;
}


