#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

#define NB_PROD 3
#define NB_CONS 3
#define NBR_CHAR 10

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int stack_size;
char pile[NBR_CHAR];

int Pop(){
  int c;

  pthread_mutex_lock(&mutex);
  while(stack_size == 0) {
    pthread_cond_wait(&cond, &mutex);
  }

  stack_size--;
  c = pile[stack_size];
  pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mutex);
  
  return c;
}

void Push(int c) {
  pthread_mutex_lock(&mutex);

  while(stack_size == NBR_CHAR) {
    pthread_cond_wait(&cond, &mutex);
  }

  pile[stack_size] = c;
  stack_size++;

  pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mutex);
}

void* Producteur(void* arg) {
    int c;
    while ((c = getchar()) != EOF) {
        Push(c);
    }
    return NULL;
}

void* Consommmateur(void* arg) {
    for (;;) {
        putchar(Pop());
        fflush(stdout);
    }
    return NULL;
}

int main() {
  pthread_t tid_prod[NB_PROD], tid_cons[NB_CONS];
  int i=0;

  stack_size = 0;

  for(i=0; i<NB_PROD; i++) {
    if(pthread_create(&tid_prod[i], NULL, Producteur, NULL) != 0) {
      printf("Erreur création producteur\n");
      exit (1);
    }
  }

  for(i=0; i<NB_CONS; i++) {
    if(pthread_create(&tid_cons[i], NULL, Consommmateur, NULL) != 0) {
      printf("Erreur création consommateur\n");
      exit (1);
    }
  }

  /* 
     Optimisation :
     
     Si un thread producteur se termine, il faudrait terminer les
     autres producteurs

   */

  for(i=0; i<NB_PROD; i++) {
    pthread_join(tid_prod[i], NULL);
  }
  
  for(i=0; i<NB_CONS; i++) {
    if(pthread_cancel(tid_cons[i]) != 0) {
      printf("erreur terminaison consommateur\n");
    }
  }

  return EXIT_SUCCESS;
}
