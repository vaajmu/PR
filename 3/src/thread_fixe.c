#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

#define  NB_THREAD 2



pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER;
char **nomFichiers;
int *etatFichiers;
int nombreDeFichiers;
void *majuscule (void* chaine){
  char *chaine2;
  FILE* fp1, *fp2;
  int i, c = 1;

  while(1){
    pthread_mutex_lock(&mutex);
    for (i=0; i<nombreDeFichiers;i++){
      if(etatFichiers[i]==0){
	chaine2=nomFichiers[i];
	etatFichiers[i]=1;
	break;
      }
    }
    
    pthread_mutex_unlock(&mutex);

    if (i==nombreDeFichiers) return NULL;

    printf("traitement de %s\n", nomFichiers[i]);
    /* sleep(2); */
    /* printf("traitement de %s\n", chaine2); */
  
    fp1= fopen (chaine2, "r");
    fp2= fopen (chaine2, "r+");
    if ((fp1== NULL) || (fp2== NULL)) {
      perror ("fopen");
      exit (1);
    }

    c = 1;
    while (c !=EOF) {
      /* printf("%c  ", c); */
      c=fgetc(fp1);
      if (c!=EOF)
	fputc(toupper(c),fp2);
    }

    fclose (fp1);
    fclose (fp2);
  }
  
}


int main (int argc, char ** argv) {
 
  int erreur,i;
  pthread_t tid[NB_THREAD];
  

  nombreDeFichiers = argc-1;
  nomFichiers=&argv[1];

  etatFichiers= (int*)(malloc(sizeof(int)*nombreDeFichiers));

  for(i=0;i<nombreDeFichiers; i++){
    etatFichiers[i]=0;
  }

  
  /* pthread_attr_init(&attr); */
  /* pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED ); */



  for (i=0; i<NB_THREAD; i++){
    if(pthread_create(&tid[i],NULL, majuscule,NULL)!=0){
      printf("erreur de création de thread...\n"); 
    }
  }
 
  
  for(i=0; i<NB_THREAD; i++){
    erreur=pthread_join(tid[i],NULL);
    if(erreur != 0){
      if(erreur==ESRCH){
	printf("thread n'existe pas\n");
      } else if (erreur==EDEADLK){
	printf("interblcage..\n");
      } else if (erreur==EINVAL){
	printf("n'est pas joignable\n");
      }
    }
  }

  free(etatFichiers);

  return EXIT_SUCCESS;
}


