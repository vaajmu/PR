#define _XOPEN_SOURCE 700
#include <stdio.h>
#include "thread_stack.h"
#include <stdlib.h>
/* #include <ctype.h> */
#include <pthread.h>
/* #include <unistd.h> */
/* #include <errno.h> */

void* Producteur(void* arg) {
    int c;
    while ((c = getchar()) != EOF) {
        Push(c);
    }
    return NULL;
}

void* Consommmateur(void* arg) {
    for (;;) {
        putchar(Pop());
        fflush(stdout);
    }
    return NULL;
}

int main() {
  pthread_t tid_prod, tid_cons;

  stack_size = 0;

  if(pthread_create(&tid_prod, NULL, Producteur, NULL) != 0) {
    printf("Erreur création producteur\n");
    exit (1);
  }

  if(pthread_create(&tid_cons, NULL, Consommmateur, NULL) != 0) {
    printf("Erreur création consommateur\n");
    exit (1);
  }

  pthread_join(tid_prod, NULL);
  
  if(pthread_cancel(tid_cons) != 0) {
    printf("erreur terminaison consommateur\n");
  }

  return EXIT_SUCCESS;
}
