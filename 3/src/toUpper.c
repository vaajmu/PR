#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

void *majuscule (void* chaine){
  char *chaine2;
  FILE* fp1, *fp2;
  int c = 1;

  printf("debut de fonction\n");

  chaine2=(char*)chaine;
  
  fp1= fopen (chaine2, "r");
  fp2= fopen (chaine2, "r+");
  if ((fp1== NULL) || (fp2== NULL)) {
    perror ("fopen");
    exit (1);
  }

  while (c !=EOF) {
    /* printf("je suis entré fichier : %s \n", chaine2); */
    c=fgetc(fp1);
    if (c!=EOF)
      fputc(toupper(c),fp2);
  }

  fclose (fp1);
  fclose (fp2);
  
  return NULL;
}

int main (int argc, char ** argv) {
 
  int erreur,i;
  pthread_t *tid;
  

  
  tid= (pthread_t*)malloc(sizeof(pthread_t)*(argc-1));
  /* pthread_attr_init(&attr); */
  /* pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED ); */


  for (i=1; i<argc; i++){
    printf("le nom de fichier%s:\n", argv[i]);
    if(pthread_create(&tid[i-1],NULL, majuscule, argv[i])!=0){
      printf("erreur de création de thread...\n"); 
    }
  }
 
  
  for(i=1; i<argc; i++){
    erreur=pthread_join(tid[i-1],NULL);
    if(erreur != 0){
      if(erreur==ESRCH){
	printf("thread n'existe pas\n");
      } else if (erreur==EDEADLK){
	printf("interblcage..\n");
      } else if (erreur==EINVAL){
	printf("n'est pas joignable\n");
      }
    }
  }

  return EXIT_SUCCESS;
}


