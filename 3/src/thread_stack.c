#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include "thread_stack.h"

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
char pile[NBR_CHAR];

int Pop() {
  int c;

  pthread_mutex_lock(&mutex);
  while(stack_size == 0) {
    pthread_cond_wait(&cond, &mutex);
  }
  stack_size--;
  c = pile[stack_size];
  pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mutex);
  
  return c;
}

void Push(int c) {
  pthread_mutex_lock(&mutex);

  while(stack_size == NBR_CHAR) {
    pthread_cond_wait(&cond, &mutex);
  }

  pile[stack_size] = c;
  stack_size++;

  pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mutex);
}

