#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>

#define N 5

int barriereThread = 0;
int barriereMain = 0;
pthread_t tabTid[N];
pthread_t tidMain;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t mutexMain = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condMain = PTHREAD_COND_INITIALIZER;

void *thread(void *i) {
  int indice = *((int*)i);
  int paramThread = indice + 1;
  
  if(indice < N) {
    if(pthread_create(&tabTid[indice + 1], NULL, thread, &paramThread) != 0) {
      printf("Erreur création thread 0\n");
      exit(1);
    }
  } else {
    pthread_mutex_lock(&mutexMain);
    barriereMain = 1;
    pthread_cond_broadcast(&condMain);
    pthread_mutex_unlock(&mutexMain);
  }
  
  pthread_mutex_lock(&mutex);
  while(barriereThread != 1) {
    pthread_cond_wait(&cond, &mutex);
  }
  pthread_mutex_unlock(&mutex);
  
  pthread_exit((void*) NULL);
}

int main() {
  int paramThread0 = 0, i, sigRecu;
  sigset_t ens;

  tidMain = pthread_self();
  
  sigemptyset(&ens);
  pthread_sigmask(SIG_SETMASK, &ens, NULL);
  
  if(pthread_create(&tabTid[0], NULL, thread, &paramThread0) != 0) {
    printf("Erreur création thread 0\n");
    exit(1);
  }

  pthread_mutex_lock(&mutexMain);
  while(barriereMain != 1) {
    pthread_cond_wait(&condMain, &mutexMain);
  }
  pthread_mutex_unlock(&mutexMain);

  printf("Tous mes descendants sont créés\n");

  sigaddset(&ens, SIGINT);
  pthread_sigmask(SIG_SETMASK, &ens, NULL);
  sigwait(&ens, &sigRecu);
  
  pthread_mutex_lock(&mutex);
  barriereThread = 1;
  pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mutex);

  for(i=0; i<N; i++) {
    if(pthread_join(tabTid[i], NULL) != 0) {
      printf("erreur join %d\n", i);
      exit(1);
    }
  }

  printf("Tous mes descendants se sont terminés\n");
  return EXIT_SUCCESS;
}



