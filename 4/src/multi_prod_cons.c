#define SVID_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include "thread_stack.h"

#define NB_PROD 3
#define NB_CONS 3

void* Producteur(void* arg) {
    int c;
    while ((c = getchar()) != EOF) {
        Push(c);
    }
    return NULL;
}

void* Consommmateur(void* arg) {
    for (;;) {
        putchar(Pop());
        fflush(stdout);
    }
    return NULL;
}

int main() {
  pthread_t tid_prod[NB_PROD], tid_cons[NB_CONS];
  int i=0;

  init_stack();

  for(i=0; i<NB_PROD; i++) {
    if(pthread_create(&tid_prod[i], NULL, Producteur, NULL) != 0) {
      printf("Erreur création producteur\n");
      exit (1);
    }
  }

  for(i=0; i<NB_CONS; i++) {
    if(pthread_create(&tid_cons[i], NULL, Consommmateur, NULL) != 0) {
      printf("Erreur création consommateur\n");
      exit (1);
    }
  }

  /* 
     Optimisation :
     
     Si un thread producteur se termine, il faudrait terminer les
     autres producteurs

   */

  for(i=0; i<NB_PROD; i++) {
    pthread_join(tid_prod[i], NULL);
  }
  
  for(i=0; i<NB_CONS; i++) {
    if(pthread_cancel(tid_cons[i]) != 0) {
      printf("erreur terminaison consommateur\n");
    }
  }

  return EXIT_SUCCESS;
}
