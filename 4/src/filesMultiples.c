#define SVID_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

#define NBPROC 5
#define MAXSIZE 50
#define MAXALEA 10

typedef struct{
  long type;
  int nb;
  int idFils;
}MessagePere;

typedef struct{
  long type;
  int nb;
} MessageFils;

int main() {
  MessageFils * tabStruct_Fils;
  int i, random_val,j, k,somme = 0;
  int a, b, c, d, e;
  pid_t pid;
  int msgId[NBPROC+1];
  MessagePere messagePere;
  MessageFils messageFils;
  
  srand(getpid());

  for(i=0;i<NBPROC+1;i++){ 
    msgId[i] = msgget(IPC_PRIVATE, 0666 | IPC_CREAT);
  }

  for(i=0; i<NBPROC; i++) {
    pid = fork();
    if(pid == -1) {
      printf("erreur fork\n");
      exit(1);
    } else if (pid == 0) {	/* fils */
      srand(getpid());

      a=i;
      random_val = (rand() % NBPROC)+1; 
      printf("proc %d veut %d nombres\n",a, random_val);
      b=i;
      messagePere.type = 1;
      messagePere.nb = random_val;
      messagePere.idFils=a;
      msgsnd(msgId[NBPROC], &messagePere, sizeof(MessagePere), 0);
      c=i;
      /* recuperer les valeur et sommer */
      for(k=0;k<random_val;k++){
	msgrcv(msgId[a],&messageFils, sizeof(MessageFils),0 ,0);
	d=i;
	printf("\t\t\t%d += %d\n", a, messageFils.nb);
	somme+=messageFils.nb;
      }
      e=i;
      printf("\t\t\t\t\t%d vaut %d\n",a,somme);
      printf("%d %d %d %d %d\n", a, b, c, d, e);
      /* avec ce dernier affichage, on remarqie que la valeur de i est
	 modifiée au niveau du msgrcv sans que nous puissions
	 l'expliquer. C'est une erreur qui n'a pas l'air d'avoir lieu
	 à l'ARI. Pour garder une valeur non corrompue de i (index du
	 processus), nous utilisons a qui ne change pas (b ou c
	 pourraient aussi être utilisés) */
      return 0;
    }

  }

  for(i=0; i<NBPROC; i++) {
    msgrcv(msgId[NBPROC], &messagePere, sizeof(MessagePere), 0, 0);
    printf("\t\t\t\t\t\t\tproc %d attends %d nombres\n", messagePere.idFils, messagePere.nb);
    tabStruct_Fils=(MessageFils*)malloc(sizeof(MessageFils)*messagePere.nb);
    for(j=0;j<messagePere.nb;j++){
      random_val = (rand() % 101);
      tabStruct_Fils[j].nb=random_val;
      tabStruct_Fils[j].type=1;
      printf("\t\t\t\t\t\t\t\t\t\t\tenvoi à %d de %d\n",messagePere.idFils,random_val);
      msgsnd(msgId[messagePere.idFils],&tabStruct_Fils[j], sizeof(MessageFils),0);
    }
    /* optimisation : liberer la memoire */
  }

  for(i=0; i<NBPROC; i++) {
    wait(NULL);
  }

  return EXIT_SUCCESS;
}
