#define SVID_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include <time.h>

#define NPROC 5

int semId;

/* arg N ??? */
void wait_barrier(int N) {
  struct sembuf op;
  
  op.sem_num = 0;
  op.sem_op = -1;
  op.sem_flg = 0;
  semop(semId, &op, 1);

  op.sem_num = 0;
  op.sem_op = 0;
  op.sem_flg = 0;
  semop(semId, &op, 1);
}

void process (int NB_PCS) {
  printf ("avant barrière\n");
  wait_barrier (NB_PCS);
  printf ("\t\t\t\taprès barrière\n");
  exit (0);
}

int main() {
  int i;
  pid_t pid;

  semId = semget(IPC_PRIVATE, 1, IPC_CREAT | 0666);
  semctl(semId, 0, SETVAL, NPROC);

  for(i=0; i<NPROC; i++) {
    /* sleep(1); */ /* Pour bien montrer qu'ils s'attendent */
    pid = fork();
    if (pid == -1) {
      printf("erreur fork\n");
      exit(1);
    } else if (pid == 0) {	/* Code fils */
      process(NPROC);
      return EXIT_SUCCESS;	/* inutile, se fait dans process */
    }
  }

  for(i=0; i<NPROC; i++) {
    wait(NULL);
  }

  return EXIT_SUCCESS;
}


