#define SVID_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/ipc.h>
#include <sys/msg.h>

#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

#define NBPROC 5
#define MAXSIZE 50
#define MAXALEA 10

/* #include <ctype.h> */
/* #include <unistd.h> */
/* #include <errno.h> */
/* #include <pthread.h> */

typedef struct {
  long type;
  int nb;
} Message;

int main() {
  int i, random_val, somme = 0;
  pid_t pid;
  //  int p = 1; 

  int msgId;
  
  Message message;
  char path[] = "fileEx1";
  key_t cle = ftok(path, 0);

  msgId = msgget(cle, 0666 | IPC_CREAT);

  for(i=0; i<NBPROC; i++) {
    pid = fork();
    if(pid == -1) {
      printf("erreur fork\n");
      exit(1);
    } else if (pid == 0) {	/* fils */
      srand(getpid());

      /* random_val = rand() % 10; */
      random_val = (int) (10*(float)rand()/ RAND_MAX);
      printf("aléa = %d\n", random_val);
      message.type = 1;
      message.nb = random_val;
      msgsnd(msgId, &message, sizeof(int), 0);
      return 0;
    }
  }

  for(i=0; i<NBPROC; i++) {
    wait(NULL);
  }

  for(i=0; i<NBPROC; i++) {
    msgrcv(msgId, &message, sizeof(int), 0, 0);
    somme += message.nb;
  }
  printf("Somme des messages : %d\n", somme);

  return EXIT_SUCCESS;
}
