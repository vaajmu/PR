#define SVID_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "thread_stack.h"

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int shmId;
/* char pile[NBR_CHAR]; */
/* int stack_size = 0; */

void init_stack() {
  char *addr = NULL;
  pthread_mutex_lock(&mutex);
  shmId = shmget(IPC_PRIVATE, NBR_CHAR, IPC_CREAT | 0666);
  addr = shmat(shmId, NULL, 0600);
  *addr = 0;			/* premiere case pour la taille */
  shmdt(addr);
  pthread_mutex_unlock(&mutex);
}

int Pop() {
  int c;
  char *addr;

  pthread_mutex_lock(&mutex);
  addr = shmat(shmId, NULL, 0600);
  while(*addr == 0) {
    pthread_cond_wait(&cond, &mutex);
  }
  (*addr)--;
  c = addr[*addr + 1];
  shmdt(addr);
  pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mutex);
  
  return c;
}

void Push(int c) {
  char *addr;

  pthread_mutex_lock(&mutex);
  addr = shmat(shmId, NULL, 0600);

  while(*addr == NBR_CHAR) {
    pthread_cond_wait(&cond, &mutex);
  }

  addr[*addr + 1] = c;
  (*addr)++;
  shmdt(addr);
  pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mutex);
}

