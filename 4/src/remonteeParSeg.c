#define SVID_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

#define NBPROC 5
#define MAXSIZE 50
#define MAXALEA 10

typedef struct {
  long type;
  int nb;
} Message;

int main() {
  int i, random_val, somme = 0, cpt = 0;
  pid_t pid;
  /* int tailleShm = 100; */
  //  int p = 1; 

  char* adrAtt;
  int shmId;
  char path[] = "fileEx1";
  key_t cle = ftok(path, 0);

  shmId = shmget(cle, NBPROC, 0666 | IPC_CREAT);

  for(i=0; i<NBPROC; i++) {
    pid = fork();
    if(pid == -1) {
      printf("erreur fork\n");
      exit(1);
    } else if (pid == 0) {	/* fils */
      srand(getpid());
  
      random_val = (int) (10*(float)rand()/ RAND_MAX);
      printf("aléa = %d\n", random_val);
      adrAtt = shmat(shmId, NULL, 0600);
      *(adrAtt+cpt) = random_val;
      shmdt(adrAtt);
      return 0;
    }
    cpt++;
  }

  for(i=0; i<NBPROC; i++) {
    wait(NULL);
  }

  adrAtt = shmat(shmId, NULL, 0600);
  for(i=0; i<NBPROC; i++) {
    somme += *(adrAtt+i);
  }
  shmdt(adrAtt);
  printf("Somme des messages : %d\n", somme);

  return EXIT_SUCCESS;
}
