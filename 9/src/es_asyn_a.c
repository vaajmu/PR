#define _XOPEN_SOURCE 700

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <aio.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>

#define TAILLE 1024

void doNothing(int sig){}

int main(int argc, char **argv){
  int retourAio,fd,fd2, nbLus;
  struct aiocb aio;
  char msg[TAILLE];
  sigset_t sigproc;
  struct sigaction action;

  if(argc!=3){
    printf("usage :%s <nom de fichier> <chaine de caractere>\n",argv[0]);
    exit(1);
  }

  if((fd=open(argv[1], O_WRONLY |O_CREAT|O_TRUNC, 0644))==-1){
    perror("open");
    exit(1);
  }

  sigemptyset(&sigproc);
  sigaddset(&sigproc, SIGRTMIN);
  sigprocmask(SIG_SETMASK, &sigproc, NULL);

  sigfillset(&sigproc);
  sigdelset(&sigproc,SIGRTMIN);

  action.sa_mask = sigproc;
  action.sa_flags = 0;
  action.sa_handler = doNothing;
  sigaction(SIGRTMIN, &action, NULL);

  aio.aio_fildes=fd;
  aio.aio_offset=0;
  aio.aio_buf=argv[2];
  aio.aio_nbytes=strlen(argv[2]);
  aio.aio_reqprio=0;
  aio.aio_sigevent.sigev_notify=SIGEV_SIGNAL;
  aio.aio_sigevent.sigev_signo =SIGRTMIN;

  if(aio_write(&aio)==-1){
    perror("aio_write");
    exit(1);
  }

  if((fd2=open(argv[1],O_RDONLY))==-1){
    perror("open 2");
    exit(1);
  }

  while((retourAio=aio_error(&aio))!=0){
    if(retourAio!=EINPROGRESS){
      perror("aio_error");
      exit(1);
    }
    printf("Attente\n");
    sigsuspend(&sigproc);
    printf("Sortie\n");
  }

  if(aio_return(&aio)==-1){
    perror("aio_return");
    exit(1);
  }

  if(close(fd)==-1){
    perror("close fd");
    exit(1);
  }

  /* aio.aio_fildes=fd2; */
  /* aio.aio_offset=0; */
  /* aio.aio_buf=msg; */
  /* aio.aio_nbytes=TAILLE; */
  /* aio.aio_reqprio=0; */
  /* aio.aio_sigevent.sigev_notify=SIGEV_SIGNAL; */
  /* aio.aio_sigevent.sigev_signo =SIGRTMIN; */

  /* if(aio_read(&aio)==-1){ */
  /*   perror("aio_read"); */
  /*   exit(1); */
  /* } */

  /* while((retourAio=aio_error(&aio))!=0){ */
  /*   if(retourAio!=EINPROGRESS){ */
  /*     perror("aio_error"); */
  /*     exit(1); */
  /*   }  */
  /*   sigsuspend(&sigproc); */
  /* } */

  /* if((nbLus = aio_return(&aio))==-1){ */
  /*   perror("aio_return"); */
  /*   exit(1); */
  /* } */

  if((nbLus=read(fd2,msg,TAILLE))==-1){
    perror("read");
    exit(1);
  }
 
  msg[nbLus] = '\0';
  printf("lu: %s\n",msg);

  if(close(fd2)==-1){
    perror("close fd");
    exit(1);
  }

  return EXIT_SUCCESS;
}
