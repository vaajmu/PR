#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NBPROC 5
#define MAXALEA 10

void doNothing(int signo, siginfo_t *si, void *context){}

void calculer(int i) {
  union sigval val;
  int alea;

  srand(getpid());
  
  alea = rand() % MAXALEA;
  printf("proc %d : aléa = %d\n", i, alea);

  val.sival_int = alea;
  sigqueue(getppid(), SIGRTMIN, val);
}

int main() {
  int i, somme = 0;
  pid_t pid;
  sigset_t set;
  struct sigaction action;
  siginfo_t info;
  
  /* Masquer SIGRTMIN pour le moment */
  if(sigemptyset(&set) == -1) {
    perror("sigemptyset ");
    exit(1);
  }
  if(sigaddset(&set, SIGRTMIN) == -1) {
    perror("sigaddset ");
    exit(1);
  }
  if(sigprocmask(SIG_SETMASK, &set, NULL) == -1) {
    perror("sigprocmask ");
    exit(1);
  }

  /* Mise en place du handler de signaux */
  action.sa_sigaction = doNothing;
  action.sa_flags = SA_SIGINFO;
  action.sa_mask = set;
  sigaction(SIGRTMIN, &action, NULL);

  for(i=0; i<NBPROC; i++) {
    if((pid = fork()) == -1) {
      perror("fork ");
      exit(1);
    } else if(pid == 0) {
      calculer(i);
      exit(0);
    }
  }

  for(i=0; i<NBPROC; i++) {
    if(sigwaitinfo(&set, &info) == -1) {
      perror("sigwait ");
      exit(1);
    }
    somme += info.si_value.sival_int;
    printf("\t\t\t\t\t\t+= %d\n", info.si_value.sival_int);
  }

  printf("\t\t\t\t\t\tTOTAL : %d\n", somme);

  return 0;
}

