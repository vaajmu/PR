#include <stdio.h>
#include <stdlib.h>

#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NPROC 5

void doNothing(int signo, siginfo_t *si, void *context) {}

void wait_barriere(int N) {
  sigset_t set;
  int signo = 0;
  union sigval val;

  /* Envoyer un signal au père */
  val.sival_int = 0;
  if(sigqueue(getppid(), SIGRTMIN, val) == -1) {
    perror("sigqueue ");
    exit(1);
  }

  /* Attendre le signal du père */
  if(sigemptyset(&set) == -1) {
    perror("sigemptyset ");
    exit(1);
  }
  if(sigaddset(&set, SIGRTMIN) == -1) {
    perror("sigaddset ");
    exit(1);
  }
  if(sigwait(&set, &signo) == -1) {
    perror("sigwait ");
    exit(1);
  }

}

void fun_fils(int id) {
  printf("\t\t\t\t%d avant\n", id);
  wait_barriere(NPROC);
  printf("\t\t\t\t%d après\n", id);
}

int main() {
  int i, signo;
  pid_t pids[NPROC];
  sigset_t set;
  struct sigaction action;
  union sigval val;

  /* Masquer le signal SIGRTMIN */
  if(sigemptyset(&set) == -1) {
    perror("sigemptyset ");
    exit(1);
  }
  if(sigaddset(&set, SIGRTMIN) == -1) {
    perror("sigaddset ");
    exit(1);
  }
  if(sigprocmask(SIG_SETMASK, &set, NULL) == -1) {
    perror("sigprocmask ");
    exit(1);
  }

  /* Déclarer sigaction */
  action.sa_sigaction = doNothing;
  action.sa_mask = set;
  action.sa_flags = SA_SIGINFO;
  if(sigaction(SIGRTMIN, &action, NULL) == -1) {
    perror("sigaction ");
    exit(1);
  }

  for(i=0; i<NPROC; i++) {
    if((pids[i] = fork()) == -1) {
      perror("fork ");
      exit(1);
    } else if(pids[i] == 0) {
      fun_fils(i);
      exit(0);
    }
  }

  printf("PERE : ATTENTE BARRIERE\n");
  for(i=0; i<NPROC; i++) {	/* Attendre N signaux */
    if(sigwait(&set, &signo) == -1) {
      perror("sigwait ");
      exit(1);
    }
  }

  printf("PERE : LIBERER FILS\n");
  val.sival_int = 0;
  for(i=0; i<NPROC; i++) {	/* Envoyer N signaux */
    if(sigqueue(pids[i], SIGRTMIN, val) == -1) {
      perror("sigqueue ");
      exit(1);
    }
  }

  printf("PERE : ATTENTE FILS\n");
  for(i=0; i<NPROC; i++) {	/* Attendre la fin des fils */
    wait(NULL);
  }

  printf("PERE : FIN\n");
  return EXIT_SUCCESS;
}

