#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>

#define _XOPEN_SOURCE 700

void doNothing(int signo, siginfo_t *si, void *context){}

int main (int argc, char ** argv) {

  pid_t pid, papa=getpid();
  int i,N;
  sigset_t set;
  struct sigaction action;
  int mysig;
  union sigval val;

  if(argc!=2){
    printf("\tusage: %s <NBPROCs>\n",argv[0]);
    exit(1);
  }

  N=atoi(argv[1]); i=1;

  printf("### PCS initial PID = %d #####\n",getpid());
  printf("\n");
  sigemptyset(&set);
  sigaddset(&set,SIGRTMIN);
  sigprocmask(SIG_SETMASK,&set,NULL);

  action.sa_flags=SA_SIGINFO;
  action.sa_sigaction=doNothing;
  /* action.sa_handler=doNothing; */
  action.sa_mask=set;
  sigaction(SIGRTMIN,&action,0);

  while(i<=N){
   
    if((pid = fork())){
      /* printf("\t\t\t i=%d PID %d: sort de la boucle\n",i,getpid());      */
      break;
    }
   
  
    /* ############## si pid == 0 ########################## */
    else if(pid == -1){
      perror("fork");
      exit(1);
      }
    i++;
  }

  if(i==N){
   printf("%d> PID : %d\n",i,getpid());

    if(getpid()!=papa){ 	/*papa ne débloque personne */
      val.sival_int = 0;
      printf("\t\tfils %d : débloquer mon père dont PID= %d\n",i,getppid());
      if(sigqueue(getppid(),SIGRTMIN, val)==-1){
	perror("sigqueue");
	exit(1);
      }
    }
  }
  
  if(i<N){
    if(sigemptyset(&set)==-1){
      perror("sigemptyset");
      exit(1);
    }
    if(sigaddset(&set,SIGRTMIN)==-1){
      perror("sigaddset");
      exit(1);
    }
    if(sigwait(&set, &mysig)==-1){ /* je suis blocké tant que je n'ai pas reçu le signal */
      perror("sigwait");
      exit(1);
    }
  
 
    /* quand le pcs est debloqué =>affiche son pid et son ordre */
    printf("%d> PID : %d\n",i,getpid());

    if(getpid()!=papa){ 	/*papa ne débloque personne */
      val.sival_int = 0;
      printf("\t\tfils %d : débloquer mon père dont PID= %d\n",i,getppid());
      if(sigqueue(getppid(),SIGRTMIN, val)==-1){
	perror("sigqueue");
	exit(1);
      }
    }
  }
  return 0;
}

