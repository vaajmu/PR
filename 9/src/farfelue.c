#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <aio.h>

#define NBCHAR 10

int main(int argc, char **argv) {
  int fdIn, fdOut;
  char buf[NBCHAR];
  int cpt=0, i, nbLus;
  struct aiocb *liste[NBCHAR];
  struct sigevent sig;
  
  /* Vérification des arguments */
  if(argc != 3) {
    printf("Usage : %s fichierIn fichierOut\n", argv[0]);
    exit(1);
  }

  /* Ouverture des fichiers */
  if((fdIn = open(argv[1], O_RDONLY)) == -1) {
    perror("open ");
    exit(1);
  }
  if((fdOut = open(argv[2], O_WRONLY | O_TRUNC | O_CREAT, 0644)) == -1) {
    perror("open ");
    exit(1);
  }

  /* Recopie */
  do {
    /* Lecture */
    if((nbLus = read(fdIn, buf, sizeof(char)*NBCHAR)) == -1) {
      perror("read ");
      exit(1);
    }

    /* Libération du tableau */
    for(i=0; i<NBCHAR; i++) {
      if((liste[i] = (struct aiocb *)malloc(sizeof(struct aiocb))) == NULL) {
	perror("malloc ");
	exit(1);
      }
    }

    /* Options de l'écriture */
    for(i=0; i<nbLus; i++) {
      liste[i]->aio_fildes = fdOut;
      liste[i]->aio_offset = cpt + i;
      liste[i]->aio_buf = &buf[nbLus-i-1];
      liste[i]->aio_nbytes = sizeof(char);
      liste[i]->aio_reqprio = 0;
      liste[i]->aio_sigevent.sigev_notify = SIGEV_NONE;
      liste[i]->aio_lio_opcode = LIO_WRITE;
    }

    /* Écriture bloquante */
    if(lio_listio(LIO_WAIT, liste, nbLus, &sig) == -1) {
      perror("lio_listio ");
      exit(1);
    }

    /* Mise à jour du nombre de char dans le fichier dst */
    cpt+=nbLus;
  } while(nbLus == NBCHAR);	/* Tant qu'il reste des chars dans le
				   fichier src */
  /* Libération du tableau */
  for(i=0; i<NBCHAR; i++) {
    free(liste[i]);
    /* ?? Libérer les ressources aio avec aio_return ?? */
  }


  /* Fermeture des fichiers */
  if(close(fdIn) == -1) {
    perror("close ");
    exit(1);
  }
  if(close(fdOut) == -1) {
    perror("close ");
    exit(1);
  }

  return 0;
}

