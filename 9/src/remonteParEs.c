
#define _XOPEN_SOURCE 700

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <aio.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <sys/wait.h>

#define NPROC 5
#define MAXALEA 10



void doNothing() {}


void ecrire(int id, char *name){
  struct aiocb aio, *lio[1];
  int fdf,alea;
  struct sigaction action;
  sigset_t set;


  action.sa_flags = 0;
  action.sa_handler = doNothing;
  sigaction(SIGRTMIN, &action, NULL);

  sigemptyset(&set);
  sigaddset(&set, SIGRTMIN);
  sigprocmask(SIG_SETMASK, &set, NULL);

  srand(getpid());
  alea= rand() % MAXALEA;

  printf("proc %d : aléa = %d\n", id, alea);

  if((fdf=open(name,O_WRONLY))==-1){
    perror("open fils");
    exit(1);
  }

  aio.aio_fildes=fdf;
  aio.aio_offset=sizeof(int)*id;
  aio.aio_buf=&alea;
  aio.aio_nbytes=sizeof(int);
  aio.aio_reqprio=0;
  aio.aio_sigevent.sigev_notify=SIGEV_SIGNAL;
  aio.aio_sigevent.sigev_signo =SIGRTMIN;

  if(aio_write(&aio)==-1){
    perror("aio_write ");
    exit(1);
  }
 
  lio[0]=&aio;
 
  if(aio_suspend((const struct aiocb * const *)&lio[0],1,0)==-1){ /* longueur de la list =0 ?? */
    perror("aio_susupend");
    exit(1);
  }

  printf("fin suspend %d\n", id);

  if(close(fdf)==-1){
    perror("close \n");
    exit(1);
  }

  printf("\t\t fin de fils %d\n",id);

}


int main(int argc, char** argv){


  int pid,i,fd,val[NPROC],somme=0;
  struct aiocb *aio[NPROC];
  struct sigaction action;

  if(argc != 2){
    printf("usage : %s <nom de fichier>\n", argv[0]);
    exit(1);
  }

  action.sa_flags = 0;
  action.sa_handler = doNothing;
  sigaction(SIGRTMIN, &action, NULL);


  if((fd=open(argv[1],O_RDWR|O_CREAT|O_TRUNC,0644))==-1){
    perror("open");
    exit(1);
  }

  for(i=0;i<NPROC;i++){

    if((pid=fork())==-1){
      perror("fork");
      exit(1);
    } 
    else if(pid==0){
      ecrire(i,argv[1]);
      return EXIT_SUCCESS;  
    }
  }

  for(i=0;i<NPROC;i++){
    wait(NULL);
  }

  printf("fin wait \n");

  for(i=0;i<NPROC;i++){
    aio[i] = (struct aiocb *)malloc(sizeof(struct aiocb));

    aio[i]->aio_fildes=fd;
    aio[i]->aio_offset=sizeof(int)*i;
    aio[i]->aio_buf=&val[i];
    aio[i]->aio_nbytes=sizeof(int);
    aio[i]->aio_reqprio=0;
    aio[i]->aio_sigevent.sigev_notify=SIGEV_SIGNAL;
    aio[i]->aio_sigevent.sigev_signo =SIGRTMIN;
  
    if(aio_read(aio[i])==-1){
      perror("aio_read pere");
      exit(1);
    }
  }

  for(i=0;i<NPROC;i++){
    printf("%d :\n", i);
    if(aio_suspend((const struct aiocb * const *)&aio[i],1,0)==-1){
      perror("aio_suspend");
      exit(1);
    }
    printf("valeur de %d = %d\n",i, val[i]);
    somme+=val[i];
  }

  if(close(fd)==-1){
    perror("close");
    exit(1);
  }

  printf("somme des valeurs envoyés par les fils =%d\n",somme);

  return EXIT_SUCCESS;
}
