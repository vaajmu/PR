#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#define N 5

int somme = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *thread_rand(void *i) {
  int cpt = *(int*)i;
  int *ret = (int*)malloc(sizeof(int));
  int alea = (int) (10*((double)rand())/ RAND_MAX);

  sleep(alea);

  pthread_mutex_lock(&mutex);
  somme += alea;
  pthread_mutex_unlock(&mutex);

  free(i);

  *ret = cpt * 2;

  printf("Ordre : %d\tPID : %d\talea = %d\n", cpt, (int)pthread_self(), alea);
  pthread_exit((void*)ret); 
  return NULL;
}

void *print_thread(void *p) {
  pthread_t *tid = (pthread_t *)p;
  int *ret_tid[N];
  int i;
  
  for(i=0; i<N; i++) {
    if(pthread_join(tid[i], (void**)&ret_tid[i]) != 0) {
      printf("erreur thread join\n");
      exit(1);
    }
    printf("Thread %d : retour : %d\n", i, *ret_tid[i]);
    free(ret_tid[i]);
  }

  printf("Somme : %d\n", somme);
  return NULL;
}

int main() {
  int i;
  pthread_t tid[N];
  pthread_t tid_print;
  int *p = NULL;

  srand(time(NULL));

  for(i=0; i<N; i++) {
    p = (int*)malloc(sizeof(i));
    *p = i;
    
    if(pthread_create(&tid[i], NULL, thread_rand, p) != 0) {
      printf("erreur creation thread\n");
      exit(1);
    }
  }

  if(pthread_create(&tid_print, NULL, print_thread, tid) != 0) {
    printf("erreur creation thread\n");
    exit(1);
  }
  
  if(pthread_join(tid_print, NULL) != 0) {
    printf("erreur thread join\n");
    exit(1);
  }

  printf("Fin main\n");

  return 0;
}

