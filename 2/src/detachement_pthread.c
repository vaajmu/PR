#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#define N 5

int somme = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex_cond = PTHREAD_MUTEX_INITIALIZER;
int tab[N];
int flag=0;

void *thread_rand(void *i) {
  int cpt = *(int*)i;
  int alea = (int) (10*((double)rand())/ RAND_MAX);

  sleep(alea);
  
  pthread_mutex_lock(&mutex);
  somme += alea;
  pthread_mutex_unlock(&mutex);

  free(i);

  tab[cpt]= cpt *2;
  printf("Ordre : %d\tTID : %d\talea = %d\n", cpt, (int)pthread_self(), alea);

  pthread_mutex_lock(&mutex_cond);
  flag++;
  pthread_cond_signal(&cond);
  pthread_mutex_unlock(&mutex_cond);

  pthread_exit((void*)NULL); 
  return NULL;
}

void *print_thread(void *p) {
  int i;
   pthread_mutex_lock(&mutex_cond);
   
    
   while(flag!=N){
     pthread_cond_wait(&cond, &mutex_cond);
   }
   pthread_mutex_unlock(&mutex_cond);
  
  for(i=0;i<N; i++){
    printf("Thread %d : retour : %d\n", i, tab[i]);
  }
  
  printf("Somme : %d\n", somme);
  return NULL;
}


int main() {
  int i;
  pthread_t tid[N];
  pthread_t tid_print;
  int *p = NULL;
  pthread_attr_t attr;

  srand(time(NULL));
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
  
  for(i=0; i<N; i++) {
    p = (int*)malloc(sizeof(i));
    *p = i;
    
    if(pthread_create(&tid[i], &attr , thread_rand, p) != 0) {
      printf("erreur creation thread\n");
      exit(1);
    }
  }

  if(pthread_create(&tid_print, NULL, print_thread, tid) != 0) {
    printf("erreur creation thread\n");
    exit(1);
  }
  
  if(pthread_join(tid_print, NULL) != 0) {
    printf("erreur thread join\n");
    exit(1);
  }

  printf("Fin main\n");

  return 0;
}

