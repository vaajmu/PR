#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#define NB_THREADS 5

int flagBarrier = 0;
pthread_mutex_t verrouBarrier = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condBarrier = PTHREAD_COND_INITIALIZER;

void wait_barrier(int n) {
  pthread_mutex_lock(&verrouBarrier);
  flagBarrier++;
  pthread_cond_broadcast(&condBarrier);
  pthread_mutex_unlock(&verrouBarrier);

  while(flagBarrier != n) {
    pthread_mutex_lock(&verrouBarrier);
    pthread_cond_wait(&condBarrier, &verrouBarrier);
    pthread_mutex_unlock(&verrouBarrier);
  }
}

void* thread_func (void *arg) {
  int alea = (int) (10*((double)rand())/ RAND_MAX);
  printf("attente de %d\n", alea);
  sleep(alea);
  printf ("avant barriere\n");
  wait_barrier (NB_THREADS);
  printf ("après barriere\n");
  pthread_exit ( (void*)0);
}

int main() {
  int i;
  pthread_t tabTid[NB_THREADS];
  srand(time(NULL));

  for(i=0; i<NB_THREADS; i++) {
    if(pthread_create(&tabTid[i], NULL, thread_func, NULL) != 0) {
      printf("Erreur création thread\n");
      exit(1);
    }
  }

  for(i=0; i<NB_THREADS; i++) {
    if(pthread_join(tabTid[i], NULL) != 0) {
      printf("Erreur join\n");
      exit(1);
    }
  }

  return 0;
}
