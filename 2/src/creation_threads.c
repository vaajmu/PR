#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#define N 5

void *thread_rand(void *i) {
  int cpt = *(int*)i;
  int *ret = (int*)malloc(sizeof(int));
  *ret = cpt * 2;

  printf("Ordre : %d\tPID : %d\n", cpt, (int)pthread_self());
  pthread_exit((void*)ret); 
  return NULL;
}

int main() {
  int i;
  pthread_t tid[N];
  int *ret_tid[N];
  int *p = NULL;

  for(i=0; i<N; i++) {
    p = (int*)malloc(sizeof(i));
    *p = i;
    
    if(pthread_create(&tid[i], NULL, thread_rand, p) != 0) {
      printf("erreur creation thread\n");
      exit(1);
    }
  }

  for(i=0; i<N; i++) {
    if(pthread_join(tid[i], (void**)&ret_tid[i]) != 0) {
      printf("erreur thread join\n");
      exit(1);
    }
    printf("Thread %d : retour : %d\n", i, *ret_tid[i]);
  }

  return 0;
}

